<?php
error_reporting(~E_NOTICE);
include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

setcookie('currentpage_getproductSetting', $current, time() + (86400 * 30), '/');

$bigArray = array();
$sql = "SELECT 
    p.id,
    p.name as product_name, 
    p.barcode, 
    pk.name as package_name,
    u.name as unit_name
FROM
    products as p,
    package as pk,
    package_unit as pu,
    unit as u
WHERE
    p.package_unit_id = pu.id
        AND pu.package_id = pk.id
        AND pu.unit_id = u.id
        AND p.status = 1 
        AND (p.name LIKE '%$searchPhrase%' OR p.barcode LIKE '%$searchPhrase%') ";

$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount";
}
$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $product_id = $r['id'];
        $sql_select_setting = "SELECT value_of_unit from product_setting WHERE product_id = $product_id";
        $rs1 = $conn->query($sql_select_setting);
        $r1 = $rs1->fetch_assoc();

        $remaining = remainingStock($product_id, $conn);
        $rows = array(
            "numrow" => $i,
            "id" => $product_id,
            "product_name" => $r['product_name'],
            "buy_price" => $r['buy_price'],
            "sell_price" => $r['sell_price'],
            "barcode" => $r['barcode'],
            "status" => $r['status'],
            "package_name" => $r['package_name'],
            "unit_name" => $r['unit_name'],
            "setting" => "<form class='form-inline'> 1 " . $r['package_name'] . " เท่ากับ "
            . "<input class='form-control form-control-line pwm_textinput_productsetting' product_id='" . $product_id . "' type='text' value=" . $r1['value_of_unit'] . " /> "
            . "" . $r['unit_name'] . "</form>",
            "action" => "<button title='Save' type='button' class='btn btn-default btn-sm save_button' >
                    <span class='glyphicon glyphicon-check'></span>
                </button>"
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}

//$_SESSION["productSetting_current"] = (int) $current;
echo json_encode($bigArray);
