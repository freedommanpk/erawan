<?php

include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

$bigArray = array();
$sql = "SELECT 
    c.id,
    c.code,
    c.name
FROM
     category as c
WHERE
     (c.name LIKE '%$searchPhrase%') ";

$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount";
}
$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $rows = array(
            "numrow" => $i,
            "category_name" => '<a href="#" class="myeditable" data-pk="' . $r['id'] . '" id="category_' . $i . '" data-type="text" >' . $r['name'] . '</a>',
            "category_code" => $r['code'],
            "action" => "<a title='Edit' class='btn btn-danger btn-sm' href='#' onclick='confirmDelete(" . $r['id'] . ")'>
                    <span class='glyphicon glyphicon-trash'></span>
                </a>"
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}
echo json_encode($bigArray);
