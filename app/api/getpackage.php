<?php

include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

$bigArray = array();
$sql = "SELECT 
    p.id,
    p.name
FROM
     package as p
WHERE
    p.status = 1
     AND  (p.name LIKE '%$searchPhrase%') ";

$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount";
}
$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $package_id = $r['id'];
//        $bArray = array();
        $text_unit_name = '';
        $ii = 0;
        $sql_get_unit = "SELECT pu.id, u.name FROM package_unit as pu, unit as u WHERE pu.unit_id = u.id AND pu.package_id = $package_id AND pu.status = 1";
        $rs1 = $conn->query($sql_get_unit);
        while ($r1 = $rs1->fetch_assoc()) {
//            $text_unit_name = ($ii == 0) ? $r1['name'] : $text_unit_name . ',' . $r1['name'];
            if ($ii == 0) {
                $text_unit_name = $r1['name'];
            } else {
                $text_unit_name = $text_unit_name . ',' . $r1['name'];
            }
            $ii++;
        }

        $rows = array(
            "numrow" => $i,
            "unit_name" => '<i>' . $text_unit_name . '<i> <a target="blank" href="?page=unit&packages_id=' . $package_id . '" class="add_new_unit" data-id="' . $package_id . '"  >[+]</a>',
            "value" => '<a href="#" class="myeditable" data-pk="' . $package_id . '" id="package_' . $i . '" data-type="text" >' . $r['name'] . '</a>',
            "action" => "<a title='Edit' class='btn btn-danger btn-sm' href='#' onclick='confirmDelete(" . $package_id . ")'>
                    <span class='glyphicon glyphicon-trash'></span>
                </a>"
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}
echo json_encode($bigArray);
