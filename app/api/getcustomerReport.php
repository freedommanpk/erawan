<?php

include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

$bigArray = array();
$sql = "SELECT 
     customers.id,
     customers.name,
     customers.phone_number,
     customers.line_id,
     customers.address
FROM
    customers,
    invoice
WHERE
   (customers.name LIKE '%$searchPhrase%') AND customers.id = invoice.customer_id  
GROUP BY customers.id 
   "; 
$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount ";
}

$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $customers_id = $r['id'];
        $rows = array(
            "numrow" => $i,
            "id" => $customers_id,
            "customers_name" => $r['name'],
            "phone_number" => $r['phone_number'],
            "line_id" => $r['line_id'],
            "address" => $r['address'],
            "action" => "
                <a title='View & Print' class='btn btn-info btn-sm' href='?page=reportByCustomerPrint&id=$customers_id'>
                    <span class='glyphicon glyphicon-list-alt'></span>
                </a> "
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
//        $bigArray['sql'] = $sql;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}
echo json_encode($bigArray);
