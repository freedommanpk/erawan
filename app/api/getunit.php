<?php

include '../../config/connection.php';
$package_id = $_GET['package_id'];

$bigArray = array();
$sql = " SELECT u.id as unit_id,u.name FROM unit as u,package_unit as pu "
        . "WHERE u.id = pu.unit_id AND pu.package_id = $package_id AND pu.status = 1 ORDER BY u.name ASC";
$rs = $conn->query($sql);
while ($r = $rs->fetch_assoc()) {
    $rows = array(
        "id" => $r['unit_id'],
        "name" => $r['name']
    );
    $bigArray[] = $rows;
}
echo json_encode($bigArray);