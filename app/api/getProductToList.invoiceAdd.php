<?php
error_reporting(~E_NOTICE); 
include '../../config/connection.php';
include '../include/function.php';
$product = $_REQUEST['product'];
$barcode = $_REQUEST['barcode'];
$type_of_search = $_REQUEST['type_of_search'];

$bigArray = array();
$sql = "SELECT 
    p.id,
    p.name as product_name,
    p.buy_price,
    p.sell_price,
    p.sell_price_package,
    p.package_unit_id,
    p.barcode,
    p.barcode_of_unit,
    p.status,
    pk.id as package_id,
    pk.name as package_name,
    u.id as unit_id,
    u.name as unit_name,
    c.name as category_name
FROM
    products as p,
    package as pk,
    package_unit as pu,
    unit as u,
    category as c
WHERE
    p.package_unit_id = pu.id
        AND pu.package_id = pk.id
        AND pu.unit_id = u.id
        AND p.category_id = c.id
        AND p.status = 1 ";

if ($type_of_search == 1) {
    $sql .= " AND (p.barcode = $barcode OR p.barcode_of_unit = $barcode)";
} else if ($type_of_search == 2) {
    $sql .= " AND p.id = $product ";
}


$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount ";
}

//echo $sql;
$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $product_id = $r['id'];
        $remaining = remainingStock($product_id, $conn);
        $rows = array(
            "numrow" => $i,
            "id" => $product_id,
            "product_name" => $r['product_name'],
            "buy_price" => $r['buy_price'],
            "sell_price" => $r['sell_price'],
            "sell_price_package" => $r['sell_price_package'],
            "barcode" => $r['barcode'],
            "barcode_of_unit" => $r['barcode_of_unit'],
            "status" => $r['status'],
            "package_name" => $r['package_name'],
            "unit_name" => $r['unit_name'],
            "category_name" => $r['category_name'],
            "remain" => $remaining
        );
        $bigArray['product'] = $rows;
        if ($type_of_search == 1 && strlen($barcode) == 10) {
            $bigArray['type_of_product'] = 'package';
            $bigArray['type_of_product_id'] = $r['package_id'];
            $bigArray['type_of_product_name'] = $rows['package_name'];
            $bigArray['type_of_search'] = $type_of_search;
        } else if ($type_of_search == 1 && strlen($barcode) == 11) {
            $bigArray['type_of_product'] = 'unit';
            $bigArray['type_of_product_id'] = $r['unit_id'];
            $bigArray['type_of_product_name'] = $rows['unit_name'];
            $bigArray['type_of_search'] = $type_of_search;
        } else if ($type_of_search == 2) {
            $bigArray['type_of_product'] = 'none';
            $bigArray['type_of_search'] = $type_of_search;
            $rowOfType1 = array(
                "id" => $r['package_id'],
                "name" => $rows['package_name'],
                "type" => 'package',
                "barcode" => $rows['barcode']
            );
            $rowOfType2 = array(
                "id" => $r['unit_id'],
                "name" => $rows['unit_name'],
                "type" => 'unit',
                "barcode" => $rows['barcode_of_unit']
            );
            $bigArray['type_of_product_object'][] = $rowOfType1;
            $bigArray['type_of_product_object'][] = $rowOfType2;
        }
    }
} else {
    $bigArray['data'] = [];
}
echo json_encode($bigArray);
