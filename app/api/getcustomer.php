<?php

include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

$bigArray = array();
$sql = "SELECT 
    id,
    name,
    phone_number,
    line_id,
    address
FROM
    customers
WHERE
    (name LIKE '%$searchPhrase%')";

$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount ";
}
$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $customers_id = $r['id'];
        $rows = array(
            "numrow" => $i,
            "id" => $customers_id,
            "customers_name" => $r['name'],
            "phone_number" => $r['phone_number'],
            "line_id" => $r['line_id'],
            "address" => $r['address'],
            "action" => "
                <a title='Edit' class='btn btn-info btn-sm' href='?page=customerEdit&id=$customers_id'>
                    <span class='glyphicon glyphicon-edit'></span>
                </a>
                <a title='Delete' class='btn btn-danger btn-sm' href='#' onclick='confirmDelete(" . $customers_id . ")'>
                    <span class='glyphicon glyphicon-trash'></span>
                </a>"
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
//        $bigArray['sql'] = $sql;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}
echo json_encode($bigArray);
