<?php

include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

$bigArray = array();
$sql = "SELECT 
    p.id,
    p.name as product_name,
    p.buy_price,
    p.sell_price,
    p.barcode,
    p.status,
    pk.name as package_name,
    u.name as unit_name,
    c.name as category_name
FROM
    products as p,
    package as pk,
    package_unit as pu,
    unit as u,
    category as c
WHERE
    p.package_unit_id = pu.id
        AND pu.package_id = pk.id
        AND pu.unit_id = u.id
        AND p.category_id = c.id
        AND p.status = 1 
        AND (p.name LIKE '%$searchPhrase%' OR p.barcode LIKE '%$searchPhrase%' OR c.name LIKE '%$searchPhrase%') ";

$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount ";
}
$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $product_id = $r['id'];
        $barcode = $r['barcode'];
        $remaining = remainingStock($product_id, $conn);
        $rows = array(
            "numrow" => $i,
            "id" => $product_id,
            "product_name" => $r['product_name'],
            "buy_price" => $r['buy_price'],
            "sell_price" => $r['sell_price'],
            "barcode" => $r['barcode'],
            "status" => $r['status'],
            "package_name" => $r['package_name'],
            "unit_name" => $r['unit_name'],
            "category_name" => $r['category_name'],
            "remain" => $remaining,
            "action" => "<a title='Stock Management' class='btn btn-default btn-sm' href='?page=stock&barcode=$barcode'>
                    <span class='glyphicon glyphicon-inbox'></span>
                </a>
                <a title='Edit' class='btn btn-info btn-sm' href='?page=productsEdit&id=$product_id'>
                    <span class='glyphicon glyphicon-edit'></span>
                </a>"
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
//        $bigArray['sql'] = $sql;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}
echo json_encode($bigArray);
