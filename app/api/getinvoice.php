<?php

include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

$bigArray = array();
$sql = "SELECT 
    i.id,
    i.invoice_id,
    i.invoice_credit,
    i.invoice_date,
    i.invoice_due_date,
    i.invoice_payment_status,
    i.invoice_type,
    c.name as customer_name
FROM
    invoice as i,
    customers as c
    
WHERE
     i.customer_id = c.id AND 
     (i.invoice_id LIKE '%$searchPhrase%')";

$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
$sql .= " ORDER BY i.date_inserted";

if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount";
}

$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $id = $r['id'];

        $rsSUM = $conn->query("SELECT sum(price) as fullprice FROM invoice_products WHERE invoice_id = $id");
        $rSUM = $rsSUM->fetch_assoc();

        if ($r['invoice_payment_status'] != 2) {
            $btn = " <a title='Return Product' class='btn btn-warning btn-sm' href='?page=invoiceReturnProduct&id=$id'>
                    <span class='glyphicon glyphicon-repeat'></span>
                </a>                
                <a title='View/Print Invoice' class='btn btn-default btn-sm' href='?page=invoiceDetail&id=$id'>
                    <span class='glyphicon glyphicon-file'></span>
                </a>
                <a title='Edit' class='btn btn-info btn-sm' href='?page=invoiceEdit&id=$id'>
                    <span class='glyphicon glyphicon-edit'></span>
                </a>";
        } else {
            $btn = "               
                <a title='View/Print Invoice' class='btn btn-default btn-sm' href='?page=invoiceDetail&id=$id'>
                    <span class='glyphicon glyphicon-file'></span>
                </a>
                <a title='Edit' class='btn btn-info btn-sm' href='?page=invoiceEdit&id=$id'>
                    <span class='glyphicon glyphicon-edit'></span>
                </a>";
        }

        $rows = array(
            "numrow" => $i,
            "id" => $r['id'],
            "invoice_id" => $r['invoice_id'],
            "invoice_credit" => $r['invoice_credit'],
            "invoice_date" => $r['invoice_date'],
            "invoice_due_date" => $r['invoice_due_date'],
            "invoice_payment_status" => getStatusText($r['invoice_payment_status']),
            "invoice_type" => $r['invoice_type'],
            "customer_name" => $r['customer_name'],
            "fullprice" => number_format($rSUM['fullprice']),
            "action" => $btn
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}
echo json_encode($bigArray);
