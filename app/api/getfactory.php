<?php

include '../../config/connection.php';
include '../include/function.php';
$current = $_REQUEST['current'];
$rowCount = $_REQUEST['rowCount'];
$searchPhrase = $_REQUEST['searchPhrase'];

$bigArray = array();
$sql = "SELECT 
    id,
    name,
    manufacturer,
    tel,
    line_id    
FROM
    factory
    
WHERE
     (name LIKE '%$searchPhrase%') AND status = 1";

$rs = $conn->query($sql);
$row_start = (($rowCount * $current) - $rowCount);
$row_end = $rowCount * $current;
$num_rows = mysqli_num_rows($rs);
if ($row_end > $num_rows) {
    $row_end = $num_rows;
}
if ($rowCount > 0) {
    $sql .= " LIMIT $row_start, $rowCount";
}
$rs = $conn->query($sql);
$i = $row_start;
if ($num_rows) {
    while ($r = $rs->fetch_assoc()) {
        $i++;
        $id = $r['id'];
        $rows = array(
            "numrow" => $i,
            "factory_name" => $r['name'],
            "manufacturer" => $r['manufacturer'],
            "tel" => $r['tel'],
            "line_id" => $r['line_id'],
            "action" => "
                <a title='detail' class='btn btn-default btn-sm' href='?page=factoryDetail&id=$id'>
                    <span class='glyphicon glyphicon-inbox'></span>
                </a>
                <a title='Edit' class='btn btn-info btn-sm' href='?page=factoryEdit&id=$id'>
                    <span class='glyphicon glyphicon-edit'></span>
                </a>"
        );
        $bigArray['rows'][] = $rows;
        $bigArray['total'] = $num_rows;
        $bigArray['current'] = (int) $current;
        $bigArray['rowCount'] = (int) $rowCount;
    }
} else {
    $bigArray['rows'] = [];
    $bigArray['total'] = 0;
    $bigArray['current'] = (int) $current;
    $bigArray['rowCount'] = (int) $rowCount;
}
echo json_encode($bigArray);
