<?php
session_start();
error_reporting(~E_NOTICE ); 
if (empty($_SESSION["user_id"])) {
    header("Location: ../login");
    die();
}
require '../config/connection.php';
$user_name = $_SESSION["user_full_name"];
$user_id = $_SESSION["user_id"];
?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Erawan - Stock Management System</title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media='screen,print'>
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- toast CSS -->
        <link href="../plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
        <!-- morris CSS -->
        <link href="../plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <!-- chartist CSS -->
        <link href="../plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/jquery.bootgrid.min.css" rel="stylesheet" media='screen,print'>
        <link href="css/bootstrap-editable.css" rel="stylesheet" media='screen,print'>
        <!-- Custom CSS -->
        <link href="css/style.css" rel="stylesheet" media='screen,print'>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

        <link href="css/custom.css" rel="stylesheet">
        <link href="../plugins/bower_components/jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="css/colors/default.css" id="theme" rel="stylesheet">  
        <link href=" https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" id="theme" rel="stylesheet">  
       
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->  
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script> 
        <script src="../plugins/bower_components/jquery-ui-1.12.1/jquery-ui.min.js"></script> 
        <script src="js/bootstrap-notify.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        
    </head>

    <body class="fix-header"> 
        <!--        <div class="preloader">
                    <svg class="circular" viewBox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>-->
        <div id="wrapper"> 
            <?php
            require 'include/navbar-header.php';
            require 'include/navbar.php';
            ?>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    $page = isset($_GET['page']) ? $_GET['page'] : 'dashboard';
                    require './include/navbar-title.php';
                    ?>
                    <div class="body">
                        <?php
                        if (is_file('./pages/' . $page . '.php')) {
                            require_once('./pages/' . $page . '.php');
                        } else {
                            echo 'Not have this page!';
                        }
                        ?>
                    </div>
                </div>
                <?php
                require './include/footer.php';
                ?>
            </div> 
        </div> 
        <?php
        require './include/modal.delete.php';
        ?>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!--Counter js -->
        <script src="../plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
        <script src="../plugins/bower_components/counterup/jquery.counterup.min.js"></script>
        <!-- chartist chart -->
        <script src="../plugins/bower_components/chartist-js/dist/chartist.min.js"></script>
        <script src="../plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
        <!-- Sparkline chart JavaScript -->
        <script src="../plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/bootstrap-editable.min.js"></script>
        <script src="js/jquery.bootgrid.min.js"></script>
        <script src="js/jquery.cookie.js"></script>
        <script src="js/custom.min.js"></script>
        <script src="js/myscript.js"></script>
        <!--<script src="js/dashboard1.js"></script>-->
        <script src="../plugins/bower_components/toast-master/js/jquery.toast.js"></script> 

    </body>

</html>
