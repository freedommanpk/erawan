function objectifyForm(formArray) {//serialize data function
    formArray = formArray.serializeArray()
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
// Cookies
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else
        var expires = "";

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

$.fn.editable.defaults.mode = 'inline';
//$.fn.editable.defaults.ajaxOptions = {type: "PUT"};

$(document).ready(function () {
    $('[data-role="datepicker"]').datepicker({
        dateFormat: "dd/mm/yy"
    });

    $('[data-toggle="tooltip"]').tooltip()

    function InitEditable(url) {
        $('.myeditable').editable({
            url: 'action/' + url + 'Edit.php'
        });
    }
    InitEditable()
    $('#grid-data-api').bootgrid({
    }).on("loaded.rs.jquery.bootgrid", function () {
        InitEditable($(this).data('editable'));

        //product_setting page
        $('.product_setting .save_button').on('click', function () {
            var $pd = $(this).closest('tr').find('[product_id]');
            var value = $pd.val();
            var product_id = $pd.attr('product_id');
            if ((value !== '/') && (value)) {
                var data = {
                    id: product_id,
                    value: value
                }
                $.ajax({
                    type: 'GET',
                    data: data,
                    url: 'action/productSettingSave.php',
                    success: function (data) {
                        if (data) {
                            $.notify({
                                message: data
                            }, {
                                type: 'info',
                                delay: 200,
                                showProgressbar: true
                            })
                        }
                    }
                });
            } else {
                alert('กรุณากรอกจำนวน');
                $pd.val('').focus();
            }
        });
    });

});