<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$page?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <a href="logout.php" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Log Out</a>
        <ol class="breadcrumb">
            <li><a href="#"><?=$page?></a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>