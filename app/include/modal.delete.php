<div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="page_item" class="modal-content" action="">
            <input type="hidden" name="id_item" id="id_item" />
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                แน่ใจมั้ย ว่าจะลบข้อมูลนี้ <br/>
                Are you sure to delete this item ?

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">OK</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="addUnit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="page_item" class="modal-content" action="">
            <input type="hidden" name="id_item" id="id_item" />
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">เพิ่ม Unit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">OK</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="addToStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="barcode-page_item" class="modal-content" action="">
            <input type="hidden" name="barcode_id" id="barcode_id" />
            <div class="modal-header" style="    background: #7460ee; ">
                <h5 class="modal-title" style="position: fixed;color: white;" id="exampleModalLabel1">Add Product To Stock Barcode: <b><span id="barcode_id_span"></span></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body font-in-modal-body">  
                <div class="form-horizontal" >
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="value">จำนวนที่รับเข้า:</label>
                        <div class="col-sm-9">
                            <input required="" class="form-control"  style="height: 35px;
                                   font-size: 28px;
                                   font-weight: 500;
                                   text-align: right;
                                   width: 80px;
                                   color: #7460ee;" type="number" name="value" id="value" value="1" /> <span id="package_name"></span>
                            <i style="size: 10px">(1 = <span id='value_of_unit'></span> <span id='unit_name'></span>)</i>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-sm-3"  for="doc">Doc:</label>  
                        <div class="col-sm-9"> 
                            <input type="text" name="doc" id="doc"   />  
                        </div>
                    </div> 
                </div> 

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">OK</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="outOfStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="barcode-page_item" class="modal-content" action="">
            <input type="hidden" name="barcode_id" id="barcode_id" />
            <div class="modal-header" style="    background: #fb4; ">
                <h5 class="modal-title" style="position: fixed;color: white;" id="exampleModalLabel1">Out Product From Stock Barcode: <b><span id="barcode_id_span"></span></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body font-in-modal-body"> 
                <div class="form-horizontal" >
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="value">จำนวนที่นำออก</label>
                        <div class="col-sm-9"> 
                            <input required="" style="height: 35px;
                                   font-size: 28px;
                                   font-weight: 500;
                                   text-align: right;
                                   width: 80px;
                                   color: #fb4;" type="number" name="value" id="value" value="1" /> <span id="unit_name"></span>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-sm-3"  for="doc">Doc</label>  
                        <div class="col-sm-9"> 
                            <input type="text" name="doc" id="doc"   />  
                        </div>
                    </div> 
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-warning">OK</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="renewToStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="barcode-page_item" class="modal-content" action="">
            <input type="hidden" name="barcode_id" id="barcode_id" />
            <div class="modal-header" style="    background: #fb4; ">
                <h5 class="modal-title" style="position: fixed;color: white;" id="exampleModalLabel1">Out Product From Stock Barcode: <b><span id="barcode_id_span"></span></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body font-in-modal-body"> 
                <div class="form-horizontal" >
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="value">จำนวนที่นำออก</label>
                        <div class="col-sm-9"> 
                            <input required="" style="height: 35px;
                                   font-size: 28px;
                                   font-weight: 500;
                                   text-align: right;
                                   width: 80px;
                                   color: #fb4;" type="number" name="value" id="value" value="1" /> <span id="unit_name"></span>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-warning">OK</button>
            </div>
        </form>
    </div>
</div>