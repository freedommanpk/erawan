<?php

function remainingStock($product_id, $conn) {
    $remaining = 0;
    $sqlSelectStock = "SELECT 
                        stock_id, product_id, type, value
                    FROM
                        manage_stock 
                    WHERE 
                       product_id = $product_id";
    $rs2 = $conn->query($sqlSelectStock);
    while ($r2 = $rs2->fetch_assoc()) {
        $type = $r2['type'];
        if ($type == 0) { //in
            $remaining = $remaining + $r2['value'];
        } else { //out 
            $remaining = $remaining - $r2['value'];
        }
    }
    return $remaining;
}

function remainingHTML($remaining, $value_of_unit, $package_name, $unit_name) {
    echo "<span title='" . $remaining . " " . $unit_name . "' data-toggle='tooltip' data-placement='bottom' >";
    echo (int) ($remaining / $value_of_unit) . ' ' . $package_name . ' ';
    echo (int) ($remaining % $value_of_unit) . ' ' . $unit_name;
    echo "</span>";
}

function countRemaining($remaining, $value_of_unit) {
    return (int) ($remaining / $value_of_unit) . '.' . (int) ($remaining % $value_of_unit);
}

function inoutOntext($status) {
    if ($status == 0) {
        return '<span class="stock-in-purple">In</span>';
    } else {
        return '<span class="stock-out-orange">Out</span>';
    }
}

function getStatusText($statusid) {
//    0= Awaiting 1 = Delivered, 2 = Paid ,3 = Paid but not deliver
    switch ($statusid) {
        case 0:
            return 'Awaiting';
            break;
        case 1:
            return 'Delivered';
            break;
        case 2:
            return 'Paid';
            break;
        case 3:
            return 'Paid but not deliver';
            break;
        case 9:
            return 'Cancellation';
            break;
    }
}
