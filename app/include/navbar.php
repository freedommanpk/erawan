<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
        </div>
        <ul class="nav" id="side-menu">
            <li style="padding: 70px 0 0;">
                <a href="." class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Dashboard</a>
            </li>
            <!--            <li>
                            <a href="?page=profile" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Profile</a>
                        </li>-->
            <li>
                <a href="?page=stock" class="waves-effect"><i class="fa  fa-archive fa-fw" aria-hidden="true"></i>Stock Management</a>
            </li>
            <li>
                <a href="?page=products" class="waves-effect"><i class="fa fa-shopping-bag fa-fw" aria-hidden="true"></i>Product</a>
            </li>
            <li>
                <a href="?page=productSetting" class="waves-effect"><i class="fa fa-cog fa-fw" aria-hidden="true"></i>Product Setting</a>
            </li>
            <li>
                <a href="?page=packages" class="waves-effect"><i class="fa  fa-gift fa-fw" aria-hidden="true"></i>Packages</a>
            </li>
            <li>
                <a href="?page=unit" class="waves-effect"><i class="fa fa-suitcase fa-fw" aria-hidden="true"></i>Unit</a>
            </li>
            <li>
                <a href="?page=category" class="waves-effect"><i class="fa  fa-cog fa-fw" aria-hidden="true"></i>Category</a>
            </li>
            <li>
                <a href="?page=factories" class="waves-effect"><i class="fa  fa-cog fa-fw" aria-hidden="true"></i>Factories</a>
            </li>
            <li>
                <a href="?page=invoice" class="waves-effect"><i class="fa fa-users fa-fw" aria-hidden="true"></i>Invoice</a>
            </li>
            <li>
                <a href="?page=customers" class="waves-effect"><i class="fa fa-users fa-fw" aria-hidden="true"></i>Customer</a>
            </li>
            <li>
                <a href="?page=report" class="waves-effect"><i class="fa fa-file fa-fw" aria-hidden="true"></i>Report</a>
            </li>
            <li>
                <a href="?page=users" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Users</a>
            </li>

            <!--            <li>
                            <a href="basic-table.html" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i>Basic Table</a>
                        </li>
                        <li>
                            <a href="fontawesome.html" class="waves-effect"><i class="fa fa-font fa-fw" aria-hidden="true"></i>Icons</a>
                        </li>
                        <li>
                            <a href="map-google.html" class="waves-effect"><i class="fa fa-globe fa-fw" aria-hidden="true"></i>Google Map</a>
                        </li>
                        <li>
                            <a href="blank.html" class="waves-effect"><i class="fa fa-columns fa-fw" aria-hidden="true"></i>Blank Page</a>
                        </li>
                        <li>
                            <a href="404.html" class="waves-effect"><i class="fa fa-info-circle fa-fw" aria-hidden="true"></i>Error 404</a>
                        </li>-->

        </ul>
        <!--        <div class="center p-20">
                    <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank" class="btn btn-danger btn-block waves-effect waves-light">Upgrade to Pro1212313</a>
                </div>-->
    </div>

</div>