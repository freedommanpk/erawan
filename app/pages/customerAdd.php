<?php
include 'include/function.php';
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form autocomplete="off" action="action/customerAdd.php" method="post" class="form-horizontal form-material">
                <input type="hidden" name='id' value="<?= $id ?>" />
                <input type="hidden" name="unit_id" id="unit_id"/> 
                <div class="form-group">
                    
                    <div class="col-sm-6">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" >
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Phone Number:</label>
                        <input type="text" class="form-control" id="phonenumber" name="phone_number" >
                    </div>               
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="name">Line id:</label>
                        <input type="text" class="form-control" id="line_id" name="line_id" >
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Address</label>
                        <input type="text" class="form-control" id="address" name="address" >
                    </div>      
                </div>
               
                <div class="row"> 

                    <div class="col-xs-12"> 
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                        <a onclick="history.back();" class="btn btn-default pull-right" style="margin-right: 10px">Cancel</a>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/productDelete.php');
    }