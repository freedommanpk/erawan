 
<?php
include 'include/function.php';
$condition = isset($_GET['condition']) ? $_GET['condition'] : '';
$date = isset($_GET['date']) ? $_GET['date'] : '';
$month = isset($_GET['month']) ? $_GET['month'] : '';
$year = isset($_GET['year']) ? $_GET['year'] : '';
$paid_status = isset($_GET['paid_status']) ? $_GET['paid_status'] : '';
?>
<style>
    .white-box{
        margin-bottom: 10px;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <form>
            <input type="hidden" value="<?= $_GET['page'] ?>" name="page"/>
            <input type="hidden" value="<?= $iv_id ?>" name="id"/>
            เงื่อนไขการค้นหา: 
            <select name="condition" class="form-control-line">
                <option <?php
                if ($condition === 'all') {
                    echo "selected";
                }
                ?> value="all">ทั้งหมด</option>
                <option <?php
                if ($condition === 'day') {
                    echo "selected";
                }
                ?> value="day">ประจำวัน</option>
                <option <?php
                if ($condition === 'month') {
                    echo "selected";
                }
                ?> value="month">ประจำเดือน</option>
                <option <?php
                if ($condition === 'year') {
                    echo "selected";
                }
                ?> value="year">ประจำปี</option>
            </select> 
            <span data-role="day_area" class="hide">
                เลือกวันที่:
                <input type="text" name="date" class="form-control-line" data-role="datepickerX" autocomplete="off" value="<?= $date ?>">
            </span>
            <span data-role="month_area" class="hide">
                เลือกเดือน: 
                <select name="month" class="form-control-line">
                    <option <?php
                    if ($month == '1') {
                        echo "selected";
                    }
                    ?> value="1">มกราคม</option>
                    <option <?php
                    if ($month == '2') {
                        echo "selected";
                    }
                    ?> value="2">กุมภาพันธ์</option>
                    <option <?php
                    if ($month == '3') {
                        echo "selected";
                    }
                    ?> value="3"> มีนาคม</option>
                    <option <?php
                    if ($month == '4') {
                        echo "selected";
                    }
                    ?> value="4"> เมษายน</option>
                    <option <?php
                    if ($month == '5') {
                        echo "selected";
                    }
                    ?> value="5"> พฤษภาคม</option>
                    <option <?php
                    if ($month == '6') {
                        echo "selected";
                    }
                    ?> value="6"> มิถุนายน</option>
                    <option <?php
                    if ($month == '7') {
                        echo "selected";
                    }
                    ?> value="7"> กรกฎาคม</option>
                    <option <?php
                    if ($month == '8') {
                        echo "selected";
                    }
                    ?> value="8"> สิงหาคม</option>
                    <option <?php
                    if ($month == '9') {
                        echo "selected";
                    }
                    ?> value="9">กันยายน </option>
                    <option <?php
                    if ($month == '10') {
                        echo "selected";
                    }
                    ?> value="10"> ตุลาคม</option>
                    <option <?php
                    if ($month == '11') {
                        echo "selected";
                    }
                    ?> value="11">พฤศจิกายน </option>
                    <option <?php
                    if ($month == '12') {
                        echo "selected";
                    }
                    ?> value="12"> ธันวาคม</option>

                </select>
            </span>
            <span data-role="year_area" class="hide">
                เลือกปี 
                <select name="year" class="form-control-line">
                    <?php
                    $sqlGetYear = "select YEAR(invoice_date) as allyear from invoice GROUP BY YEAR(invoice_date) ORDER by invoice_date";
                    $rsGetYear = $conn->query($sqlGetYear);
                    while ($rGetYear = $rsGetYear->fetch_assoc()) {
                        if ($year == $rGetYear['allyear']) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option <?= $selected ?> value="<?= $rGetYear['allyear'] ?>"><?= $rGetYear['allyear'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </span>
            <span>
                สถานะใบสั่งซื้อ: 
                <select name="paid_status" class="form-control-line">
                    <option <?php
                    if ($paid_status === '9999') {
                        echo "selected";
                    }
                    ?> value="9999">ทั้งหมด</option>
                    <option <?php
                    if ($paid_status === '0') {
                        echo "selected";
                    }
                    ?> value="0">Awaiting </option>
                    <option <?php
                    if ($paid_status === '1') {
                        echo "selected";
                    }
                    ?> value="1">Delivered</option>
                    <option <?php
                    if ($paid_status === '2') {
                        echo "selected";
                    }
                    ?> value="2">Paid</option>
                    <option <?php
                    if ($paid_status === '3') {
                        echo "selected";
                    }
                    ?> value="3">Paid but not deliver</option>
                    <option <?php
                    if ($paid_status === '4') {
                        echo "selected";
                    }
                    ?> value="4">Delivered but not Paid</option>
                </select>
            </span>
            <button class="btn btn-sm" type="submit">ค้นหา</button>
        </form>

    </div>
    <div class="col-sm-12">
        <div class="white-box" id='DivIdToPrint'>  
            <div class="row">
                <div class="col-sm-12">
                    <table style="width: 100%">
                        <td>
                            <h2><b>ร้านเอราวัณ พลาสติก</b></h2>
                            <div>
                                <strong>ที่อยู่</strong> : 56/12 ถ.ระนอง ต.ตราดเหนือ อ.เมือง จ.ภูเก็ต 83000
                            </div>
                            <div>
                                <strong>เบอร์โทรศัพท์</strong> : 08-3521-6565
                            </div>
                        </td>
                        <td align="right">
                            <div style="text-align: right" >
                                <h3><b>รายงานสรุปยอดการสั่งซื้อ <br/>
                                        <?php
                                        if ($condition == 'month') {
                                            echo "ประจำเดือน <span id='monthTxt'></span> ปี " . $year;
                                        }
                                        if ($condition == 'year') {
                                            echo "ประจำปี" . $year;
                                        }
                                        if ($condition == 'day') {
                                            echo "ประจำวันที่ " . date('d/m/Y', strtotime($date));
                                        }
                                        ?>

                                    </b>
                                </h3>
                            </div>
                        </td>
                    </table>
                </div>
            </div>  

            <div style="padding-top: 40px"> 
                <!--                <div style="background-color: #ecf0f4;
                                     padding: 6px;
                                     font-size: 16px;
                                     font-weight: bold;">
                                    BILL TO.
                                </div>-->
                <?php
                if (!$condition) {
                    ?>
                    <div class="panel panel-primary">
                        <div class="panel-body">กรุณาเลือกเงื่อนไขการค้นหา</div>
                    </div>
                    <?php
                } else {
                    ?>
                    <table class="table table-bordered table-responsive" id='tbListOfInvoice'>
                        <thead>
                            <tr style="background-color: #ecf0f4;font-weight: bold">
                                <th style="width:2%">#</th>
                                <th style="width:30%">เลขที่ใบสั่งซื้อ</th>
                                <th style="width:14%">วันที่สั่งซื้อ</th>
                                <th style="width:14%">สถานะ</th>
                                <th style="width:8%">ราคารวม</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "SELECT id, invoice_id, customer_id, invoice_date, invoice_payment_status, 
    invoice_type, invoice_credit, invoice_due_date, note, date_inserted, 
    date_last_update, userId FROM invoice WHERE invoice_date != '' ";
                            if ($paid_status != 9999) {
                                $sql .= " AND invoice_payment_status = $paid_status";
                            }

                            if ($condition == 'year') {
                                $sql .= " AND YEAR(invoice_date) = '$year'";
                            } else if ($condition == 'month') {
                                $sql .= " AND MONTH(invoice_date) = '$month' AND YEAR(invoice_date) = '$year'";
                            } else if ($condition == 'day') {
                                $sql .= " AND invoice_date = '$date'";
                            }


                            $sql .= " ORDER BY invoice_date ASC";
//                            echo $sql;
                            $rs = $conn->query($sql);
                            $i = 1;
                            $total = 0;
                            while ($r = $rs->fetch_assoc()) {
                                $invoice_id = $r['id'];
                                $sqlIV = "SELECT sum(price) as sumprice from invoice_products where invoice_id = $invoice_id";

                                $rsIV = $conn->query($sqlIV);
                                $rIV = $rsIV->fetch_assoc();
                                ?>
                                <tr data-rol-product="">
                                    <td><?= $i ?></td>
                                    <td>  
                                        <div style="cursor: pointer" data-clicktoiv="<?= $r['id'] ?>"><?= $r['invoice_id'] ?> </div>
                                    </td>
                                    <td>
                                        <?= date('d/m/Y', strtotime($r['invoice_date'])) ?>
                                    </td>
                                    <td>
                                        <?= getStatusText($r['invoice_payment_status']) ?>
                                    </td>
                                    <td>
                                        <?= number_format($rIV['sumprice']) ?>
                                    </td> 

                                </tr>
                                <?php
                                $total = $total + (int) $rIV['sumprice'];
                                $i++;
                            }
                            ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="1"></td>

                                <td colspan="4" style="text-align: right">
                                    <span >
                                        รวมเป็นเงิน <?= number_format($total, 2) ?> บาท
                                    </span>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php
                }
                ?>

            </div>
        </div> 
        <div class="row">
            <div class="col-sm-6">
                <div class="left" style="text-align: right">
                    <button onclick='printDiv();' id="Print" type="button" class="btn btn-danger">Print</button>
                </div>
            </div> 
        </div>
    </div>
    <script>
        function hideShowCondition(val) {
            if (val == 'month') {
                $('[data-role="day_area"]').addClass('hide')
                $('[data-role="month_area"]').removeClass('hide')
                $('[data-role="year_area"]').removeClass('hide')
                $('#monthTxt').text($('[name="month"] option:selected').text())
            } else if (val == 'year') {
                $('[data-role="day_area"]').addClass('hide')
                $('[data-role="month_area"]').addClass('hide')
                $('[data-role="year_area"]').removeClass('hide')
                $('#monthTxt').text($('[name="month"] option:selected').text())
            } else if (val == 'day') {
                $('[data-role="day_area"]').removeClass('hide')
                $('[data-role="month_area"]').addClass('hide')
                $('[data-role="year_area"]').addClass('hide')
            } else {
                $('[data-role="day_area"]').addClass('hide')
                $('[data-role="month_area"]').addClass('hide')
                $('[data-role="year_area"]').addClass('hide')
            }
        }
        $(document).ready(function () {

            $('[data-clicktoiv]').click(function () {
                let val = $('[data-clicktoiv]').attr('data-clicktoiv');
                window.open('?page=invoiceDetail&id=' + val + '', '_blank');
            })

            hideShowCondition($('[name="condition"]').val())
            $('[name="condition"]').change(function () {
                var val = $(this).val();
                hideShowCondition(val)
            })

            $('[data-role="datepickerX"]').datepicker({
                dateFormat: 'yy-mm-dd'
            })
        });
        function printDiv()
        {
            var divToPrint = document.getElementById('DivIdToPrint');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><head> <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="print"> <link href="css/jquery.bootgrid.min.css" rel="stylesheet" media="print"> </head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 10);
        }

    </script>