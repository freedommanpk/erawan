<?php
include 'include/function.php';
if (!isset($_GET['id'])) {
    exit();
} else {
    $iv_id = $_GET['id'];
}
$sql = "SELECT id, invoice_id, customer_id, invoice_date, invoice_payment_status, 
    invoice_type, invoice_credit, invoice_due_date, note, date_inserted, 
    date_last_update, userId FROM invoice WHERE id = $iv_id; ";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();

$ivCustomer = $r['customer_id'];
$invoice_type = $r['invoice_type'];
$invoice_due_date = $r['invoice_due_date'];

$sqlCustomerByIv = "SELECT * FROM customers WHERE id = $ivCustomer";
$rsCustomerByIv = $conn->query($sqlCustomerByIv);
$rCustomerByIv = $rsCustomerByIv->fetch_assoc();


$sqlProduct = "select
	p.id,
	p.name,
	p.sell_price,
	p.sell_price_package,
	pa.name as package_name,
	u.name as unit_name
from
	products as p,
	package_unit as pu,
	unit as u,
	package as pa
where
	p.package_unit_id = pu.id
	and pu.package_id = pa.id
	and pu.unit_id = u.id";

$rsProduct = $conn->query($sqlProduct);
?>
<style>
    .white-box{
        margin-bottom: 10px;
    }
</style>
<div class="row">
    <form name="invoiceDetail" onsubmit="return false">

        <div class="col-sm-12">
            <div class="white-box">  
                <div class="row">
                    <div class="col-sm-8">
                        <h2><b>INVOICE ID.</b>  <?= $r['invoice_id'] ?> </h2>
                    </div>
                    <div class="col-sm-4 right"> 
                        <h2><b> [<?= getStatusText($r['invoice_payment_status']) ?>]</b> </h2>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-12">
                        <h4>ชื่อ.  <?= $rCustomerByIv['name'] ?> </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        โทร. <?= $rCustomerByIv['phone_number'] ?> 
                    </div>
                    <div class="col-sm-4">
                        ไลน์. <?= $rCustomerByIv['line_id'] ?> 
                    </div>
                    <div class="col-sm-4">
                        ที่อยู่. <?= $rCustomerByIv['address'] ?> 
                    </div>
                </div>
            </div>

        </div> 
        <div class="devider"></div>
        <div class="col-sm-12">
            <div class="white-box">
                <div> 
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="date">Date</label>
                            <input type="text" name="date" id="date" class="form-control" data-role="datepicker" autocomplete="off" value="<?= date('d/m/Y', strtotime($r['invoice_date'])) ?>">
                        </div>
                        <div class="col-sm-4">
                            <label for="date">Invoice Type</label>
                            <select class="form-control" id="term"  name="term" >
                                <option <?php
                                if ($invoice_type == 'Cash') {
                                    echo "selected";
                                }
                                ?> value="Cash"> Cash </option>
                                <option <?php
                                if ($invoice_type == 'Transfer') {
                                    echo "selected";
                                }
                                ?>  value="Transfer"> Transfer </option> 
                                <option <?php
                                if ($invoice_type == 'Cheque') {
                                    echo "selected";
                                }
                                ?>  value="Cheque"> Cheque </option> 
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label for="date">Due date</label>  
                            <input type="text" name="duedate" class="form-control" data-role="datepicker" autocomplete="off" value="<?= $invoice_due_date == '0000-00-00' ? '' : date('d/m/Y', strtotime($invoice_due_date)) ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </form>
    <div class="col-sm-12">
        <div class="white-box">
            <div> 
                <div class="row">
                    <form name="search_product" onsubmit="return false;">
                        <div class="col-sm-2">
                            <select class="form-control" id="type_of_search"  name="type_of_search" >
                                <option value="1"> Barcode </option>
                                <option value="2"> Search </option> 
                            </select>
                        </div>
                        <div class="col-sm-10">
                            <div id="showBarcode">
                                <input type="number" data-toggle="tooltip" data-placement="top" autocomplete="off" 
                                       title="Please Key Barcode or Scan Barcode and presss 'Enter' to Search the Product" 
                                       class="form-control barcode-input barcode-input-for-add-iv" id="barcode" name="barcode" value="">
                            </div>
                            <div id="showListing" class="hide">
                                <select class="form-control" id="product"  name="product" >
                                    <option value="">--- Select ---</option>
                                    <?php
                                    while ($rProduct = $rsProduct->fetch_assoc()) {
                                        ?>
                                        <option value="<?= $rProduct['id'] ?>" data-id="<?= $rProduct['id'] ?>" data-name="<?= $rProduct['name'] ?>" data-sell-price="<?= $rProduct['sell_price'] ?>" data-sell-price-package="<?= $rProduct['sell_price_package'] ?>" data-package-name="<?= $rProduct['package_name'] ?>" data-unit-name="<?= $rProduct['unit_name'] ?>"><?= $rProduct['name'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <form name="productOfInvoice" onsubmit="return false">
                    <table class="table table-bordered table-responsive" id='tbListOfInvoice'>
                        <thead>
                            <tr>
                                <th style="width:2%">#</th>
                                <th style="width:30%">สินค้า</th>
                                <th style="width:14%">รายละเอียด</th>
                                <th style="width:8%">จำนวน</th>
                                <th style="width:10%">หน่วย</th>
                                <th style="width:10%">ราคาต่อหน่วย</th>
                                <th style="width:10%">รวมเป็นเงิน</th>
                                <th style="width:10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sqlListIvProduct = "SELECT * FROM invoice_products WHERE invoice_id = $iv_id";
                            $rsListIvProduct = $conn->query($sqlListIvProduct);
                            $i = 1;
                            while ($rListIvProduct = $rsListIvProduct->fetch_assoc()) {
                                $product_id = $rListIvProduct['product_id'];
                                $sqlProductName = "SELECT name from products where id = $product_id";
                                $rsProduct = $conn->query($sqlProductName);
                                $rProduct = $rsProduct->fetch_assoc();
                                $product_barcode = $rListIvProduct['product_barcode'];

                                $sqlProReturn = "SELECT * from product_return where barcode = '$product_barcode' and invoice_id = '$iv_id'";
                                $rsProRe = $conn->query($sqlProReturn);
                                $rProRe = $rsProRe->fetch_assoc();
                                ?>
                                <tr data-rol-product="">
                                    <td><?= $i ?></td>
                                    <td>
                                        <input type="hidden" name="selected_barcode" value="<?= $rListIvProduct['product_barcode'] ?>">
                                        <input type="hidden" name="selectedproduct_id" value="<?= $rListIvProduct['product_id'] ?>">
                                        <?= $rProduct['name'] ?>

                                        <?php
                                        if ($rProRe['id']) {
                                            ?>
                                            <br/><span style="color:orange;font-size: 12px">ส่งคืนสินค้า จำนวน <?= $rProRe['return_value'] . ' ' . $rProRe['unit'] ?></span>

                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <input type="text" name="selected_description" value="<?= $rListIvProduct['description'] ?>">
                                    </td>
                                    <td>
                                        <input type="number" name="selected_value"  value="<?= $rListIvProduct['value'] ?>">
                                    </td>
                                    <td>
                                        <input type="hidden" name="selected_unit"  value="<?= $rListIvProduct['unit'] ?>">
                                        <?= $rListIvProduct['unit'] ?>
                                    </td>
                                    <td>
                                        <input type="hidden" name="selected_unit_price"   value="<?= $rListIvProduct['unit_price'] ?>">
                                        <?= $rListIvProduct['unit_price'] ?>
                                    </td>
                                    <td>
                                        <input type="number" name="price"   value="<?= $rListIvProduct['price'] ?>">
                                    </td>
                                    <td>
                                        <?php
                                        if ($r['invoice_payment_status'] == '1') {
                                            ?>
                                            <!--<button class="glyphicon glyphicon-cloud-download" data-btn="return"></button>-->
                                            <!--<button class="btn btn-sm" data-btn="return"><</button>-->
                                            <?php
                                        }
                                        ?>
                                        <button class="glyphicon glyphicon-trash" data-btn="delete"></button>

                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="5"><textarea class="form-control" name="note" placeholder="หมายเหตุ/บันทึก/เพิ่มเติม"><?= $r['note'] ?></textarea></td>
                                <td colspan="2"><input type="hidden" name="total" /><span data-name="total"></span> บาท</td>
                            </tr>
                        </tfoot>
                    </table>
                </form>
                <div class="row">
                    <div class="col-sm-6">
                        <!--0= Awaiting 1 = Delivered, 2 = Paid ,3 = Paid but not deliver-->
                        <div class="left" style="text-align: left">
                            <?php
                            if ($r['invoice_payment_status'] != '2' && $r['invoice_payment_status'] != '9') {
                                ?>
                                <button data-status="9" id="Delete" type="button" class="btn btn-danger">Cancellation This Invoice</button>
                                <?php
                            }
                            if ($r['invoice_payment_status'] == '0') {
                                ?>
                                <button data-status="3" id="PaidNotDelivered" type="button" class="btn btn-success">Paid but not deliver</button>
                                <?php
                            }
                            if ($r['invoice_payment_status'] == '0' || $r['invoice_payment_status'] == '3') {
                                ?>
                                <button data-status="1" id="Delivered" type="button" class="btn btn-success">Delivered</button>
                                <?php
                            }
                            if ($r['invoice_payment_status'] == '1') {
                                ?>
                                <button data-status="2" id="Paid" type="button" class="btn btn-success">Paid</button>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                    if ($r['invoice_payment_status'] == '0') {
                        ?>
                        <div class="col-sm-6">
                            <div class="right" style="text-align: right">
                                <button id="save" type="button" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//    function addValue(id, package_name, unit_name, value_of_unit) {
    function renewToStock() {
        $('#renewToStock').modal();
//        if (value_of_unit) {
//            $('#addToStock').modal();
//            $('#addToStock #value').focus();
//            $('#addToStock #barcode_id').val(id);
//            $('#addToStock #barcode_id_span').text(id);
//            $('#addToStock #package_name').text(package_name);
//            $('#addToStock #unit_name').text(unit_name);
//            $('#addToStock #value_of_unit').text(value_of_unit);
//            $('#addToStock #barcode-page_item').attr('action', 'action/stockAdd.php');
//        } else {
//            alert('Not have Value of Unit');
//        }
    }
    function calculate() {
        var total = 0;
        var $area = $('#tbListOfInvoice');
        $('[name="price"]', $area).each(function (i, d) {
            var price = parseInt($(this).val());
            total = total + price;
        });
        $('[name="total"]').val(total);
        $('[data-name="total"]').text(total);
    }
    function runItem() {
        var $area = $('#tbListOfInvoice');
        $('tr[data-rol-product]', $area).each(function (i, d) {
            $(this).find('td:first').html(i + 1);
        });
    }
    $(document).ready(function () {
        calculate();
        function checkNumberInput(val) {
            return typeof val === 'number' ? true : false;
        }
        function calculatewithvalue($self) {
            var $tr = $self.closest('tr');
            var value = parseInt($self.val());
            var priceofunit = parseInt($self.closest('td').next().next().text())
            $tr.find('[name="price"]').val(priceofunit * value);
            calculate();
        }
        $('button#Delivered,button#Paid,button#Delete,button#PaidNotDelivered').click(function () {
            var ivID = <?= $iv_id ?>;
            var status = $(this).data('status');
            $.ajax({
                type: 'POST',
                data: {id: ivID, status: status},
                url: 'action/invoiceEditStatus.php',
                success: function (data) {
                    // ถ้า status เป็น 1 ให้ไปอัพเดต stock อัตโนมัติ

                    if (data == '1') {
                        $.notify({
                            message: 'Update Status done, Please Wait...'
                        });

                        if (status == 1) {
                            var lengthOfProductList = $('form[name="productOfInvoice"] tbody tr').length;
                            $('form[name="productOfInvoice"] tbody tr').each(function (i) {
                                i++;
                                var $self = $(this);
                                var updatestockData = {
                                    barcode_id: $self.find('[name="selected_barcode"]').val(),
                                    id: $self.find('[name="selectedproduct_id"]').val(),
                                    value: $self.find('[name="selected_value"]').val(),
                                    doc: '<?= $r['invoice_id'] ?>'
                                };
                                $.ajax({
                                    type: 'GET',
                                    data: updatestockData,
                                    url: 'action/stockOutFromInvoice.php',
                                    success: function (data) {
                                        if (i === lengthOfProductList) {
                                            if (data == '1') {
                                                $.notify({
                                                    message: 'Update Stock Done, Please Wait...'
                                                }, {
                                                    type: 'info',
                                                    delay: 200,
                                                    showProgressbar: true,
                                                    onClose: function () {
                                                        location.reload();
                                                    }
                                                });
                                            } else {
                                                alert('Error');
                                            }
                                        }
                                    }
                                });
                            });
                        }


//                        , {
//                            type: 'info',
//                            delay: 100,
//                            showProgressbar: true,
//                            onClose: function () {
//                                if (data.status == 1) {
//                                }
////                                location.reload();
//                            }
//                        }
                    } else {
                        alert('Error');
                    }
                }
            });
        });
        $('button#save').click(function () {
            var $list = $('[data-rol-product]');
            var productList = [];
            var total = $('[name="total"]').val();
            var note = $('[name="note"]').val();
            if ($list.length) {
                $('[data-rol-product]').each(function () {
                    var $l = $(this);
                    var product = {
                        id: $l.find('[name="selectedproduct_id"]').val(),
                        product_barcode: $l.find('[name="selected_barcode"]').val(),
                        description: $l.find('[name="selected_description"]').val(),
                        value: $l.find('[name="selected_value"]').val(),
                        unit: $l.find('[name="selected_unit"]').val(),
                        unit_price: $l.find('[name="selected_unit_price"]').val(),
                        price: $l.find('[name="price"]').val()
                    }
                    productList.push(product);
                });
                var invoice = {
                    customer: $('[name="customer"]').val(),
                    date: $('[name="date"]').val(),
                    term: $('[name="term"]').val(),
                    duedate: $('[name="duedate"]').val(),
                    productList: productList,
                    note: note,
                    total: total,
                    id: <?= $iv_id ?>
                };
                console.log(invoice);
                $.ajax({
                    type: 'POST',
                    data: invoice,
                    url: 'action/invoiceEdit.php',
                    success: function (data) {
                        if (data == '1') {
                            $.notify({
                                message: 'Update this invoice successful, Please Wait...'
                            }, {
                                type: 'warning',
                                delay: 200,
                                showProgressbar: true,
                                onClose: function () {
                                    location.reload();
                                }
                            })
                        } else {
                            alert('Error');
                        }
                    }
                });
            }
        });
//        $('[name="productOfInvoice"]').submit(function () {
//            var $this = $(this);
//            console.log(objectifyForm($this))
//        });


        $('body').on('change', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('click', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('keyup', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('change', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }

        });
        $('body').on('click', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }
        });
        $('body').on('keyup', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }
        });
        $('body').on('click', '[data-btn="return"]', function () {
            $('#renewToStock').modal();
//            var dialog = '<div class="row"><div class="col-sm-12">sdsdsd</div></div>';
//            var $d = $(dialog).dialog({
//                title: 'สินค้าตีคืน'
//            });
//            $d.find('button').on('click', function () {
//                var $self = $(this);
//                $.each(typeofpd, function (i, d) {
//                    if (d.type === $self.data('type')) {
//                        $d.dialog('close');
//                        var unit_price = 0;
//                        $tr += '<tr data-rol-product>';
//                        $tr += '<td>' + (+$('[data-rol-product]').length + 1) + '</td>';
//                        $tr += '<td><input type="hidden" name="selected_barcode" value="' + d.barcode + '"> <input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
//                        $tr += '<td><input type="text" name="selected_description"></td>';
//                        $tr += '<td><input type="number" name="selected_value" value="1"></td>';
//                        $tr += '<td><input type="hidden" name="selected_unit" value="' + d.name + '" >';
//                        $tr += '' + d.name + '';
//                        $tr += '</td>';
//                        if (d.type == 'package') {
//                            unit_price = +r.product.sell_price_package;
//                        } else {
//                            unit_price = +r.product.sell_price;
//                        }
//
//                        $tr += '<td> <input type="hidden" name="selected_unit_price" value="' + unit_price + '" >' + unit_price + '</td>';
//                        $tr += '<td><input type="number" name="price" value="' + unit_price + '"></td>';
//                        $tr += '<td> <button class="glyphicon glyphicon-trash" data-btn="delete"></button><button class="glyphicon glyphicon-cloud-download" data-btn="return"></button></td>';
//                        $tr += '</tr>';
//                        $('#tbListOfInvoice tbody').append($($tr).data(productObject));
//                        calculate();
//                    }
//                });
//            });
        });

        $('body').on('click', '[data-btn="delete"]', function () {
            $(this).closest('tr').remove();
            calculate();
            runItem();
        });
        $('#customer').select2();
        $('#type_of_search').change(function () {
            var value = $(this).val();
            if (+value === 2) {
                $('#showBarcode').addClass('hide');
                $('#showListing').removeClass('hide');
                if (!$('#product').hasClass('select2-hidden-accessible')) {
                    setTimeout(function () {
                        $('#product').select2();
                    }, 100);
                }

            } else {
                $('#showBarcode').removeClass('hide');
                $('#showListing').addClass('hide');
            }
        });
        $('#product').on("select2:select", function (e) {
            var data = $(this).find(':selected').data();
        });
        $('form[name="search_product"]').submit(function () {
            var data = objectifyForm($(this));
            $.ajax({
                type: 'GET',
                data: data,
                dataType: 'json',
                url: 'api/getProductToList.invoiceAdd.php',
                success: function (r) {
//                    console.log(r)
                    var productObject = {
                        id: r.product.id,
                        type: r.type_of_product,
                        typeid: r.type_of_product_id
                    };
                    var $tr = '';
                    if (r.type_of_search == 1) {
                        var unit_price = 0;
                        $tr += '<tr data-rol-product>';
                        $tr += '<td>' + (+$('[data-rol-product]').length + 1) + '</td>';
                        $tr += '<td><input type="hidden" name="selected_barcode" value="' + data.barcode + '"> <input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
                        $tr += '<td><input type="text" name="selected_description"></td>';
                        $tr += '<td><input type="number" name="selected_value" value="1"></td>';
                        $tr += '<td><input type="hidden" name="selected_unit" value="' + r.type_of_product_name + '" >';
                        $tr += '' + r.type_of_product_name + '';
                        $tr += '</td>';
                        if (r.type_of_product == 'package') {
                            unit_price = +r.product.sell_price_package;
                        } else {
                            unit_price = +r.product.sell_price;
                        }

                        $tr += '<td><input type="hidden" name="selected_unit_price" value="' + unit_price + '" >' + unit_price + '</td>';
                        $tr += '<td><input type="number" name="price" value="' + unit_price + '"></td>';
                        $tr += '<td> <button class="glyphicon glyphicon-trash" data-btn="delete"></button><button class="glyphicon glyphicon-cloud-download" data-btn="return"></button></td>';
                        $tr += '</tr>';
                        $('#tbListOfInvoice tbody').append($($tr).data(productObject));
                        calculate();
                    } else {
                        var typeofpd = r.type_of_product_object;
                        var _package = typeofpd[0];
                        var _unit = typeofpd[1];
                        var bt1 = '<button style="width: 100%;font-size:25px;" class="btn btn-primary btn-lg" data-type=' + _package.type + '>' + _package.name + '</button>';
                        var bt2 = '<button style="width: 100%;font-size:25px;" class="btn btn-primary btn-lg" data-type=' + _unit.type + '> ' + _unit.name + '</button>';
                        var dialog = '<div class="row"><div class="col-sm-6">' + bt1 + '</div><div class="col-sm-6">' + bt2 + '</div></div>';
                        var $d = $(dialog).dialog({
                            title: 'Please choose unit of product'
                        });
                        $d.find('button').on('click', function () {
                            var $self = $(this);
                            $.each(typeofpd, function (i, d) {
                                if (d.type === $self.data('type')) {
                                    $d.dialog('close');
                                    var unit_price = 0;
                                    $tr += '<tr data-rol-product>';
                                    $tr += '<td>' + (+$('[data-rol-product]').length + 1) + '</td>';
                                    $tr += '<td><input type="hidden" name="selected_barcode" value="' + d.barcode + '"> <input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
                                    $tr += '<td><input type="text" name="selected_description"></td>';
                                    $tr += '<td><input type="number" name="selected_value" value="1"></td>';
                                    $tr += '<td><input type="hidden" name="selected_unit" value="' + d.name + '" >';
                                    $tr += '' + d.name + '';
                                    $tr += '</td>';
                                    if (d.type == 'package') {
                                        unit_price = +r.product.sell_price_package;
                                    } else {
                                        unit_price = +r.product.sell_price;
                                    }

                                    $tr += '<td> <input type="hidden" name="selected_unit_price" value="' + unit_price + '" >' + unit_price + '</td>';
                                    $tr += '<td><input type="number" name="price" value="' + unit_price + '"></td>';
                                    $tr += '<td> <button class="glyphicon glyphicon-trash" data-btn="delete"></button><button class="glyphicon glyphicon-cloud-download" data-btn="return"></button></td>';
                                    $tr += '</tr>';
                                    $('#tbListOfInvoice tbody').append($($tr).data(productObject));
                                    calculate();
                                }
                            });
                        });
                    }
//                    console.log(data);
//                    if (data) {
//                        $.notify({
//                            message: data
//                        }, {
//                            type: 'info',
//                            delay: 200,
//                            showProgressbar: true
//                        })
//                    }
                    $('[name="barcode"]').val('');
                }
            });
//            var data = '';
//            if (values.type_of_search == 1) {
//                data = values.barcode;
//            } else if (values.type_of_search == 2) {
//                data = values.product;
//            }
        });
        $('#product').change(function () {
            $('form[name="search_product"]').trigger('submit');
        });
        $('#type_of_search').change(function () {
            $('#barcode').val('');
            $('#product').prop("selectedIndex", 0).trigger('change.select2');
        });
    });
</script>