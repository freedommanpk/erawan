<?php
include 'include/function.php';
if (!isset($_GET['id'])) {
    exit();
} else {
    $iv_id = $_GET['id'];
}
$sql = "SELECT id, invoice_id, customer_id, invoice_date, invoice_payment_status, 
    invoice_type, invoice_credit, invoice_due_date, note, date_inserted, 
    date_last_update, userId FROM invoice WHERE id = $iv_id; ";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();

$ivCustomer = $r['customer_id'];
$invoice_type = $r['invoice_type'];
$invoice_due_date = $r['invoice_due_date'];

$sqlCustomerByIv = "SELECT * FROM customers WHERE id = $ivCustomer";
$rsCustomerByIv = $conn->query($sqlCustomerByIv);
$rCustomerByIv = $rsCustomerByIv->fetch_assoc();
?>
<style>
    .white-box{
        margin-bottom: 10px;
    }
</style>
<div id='DivIdToPrint'>
    <div class="col-sm-12">
        <div class="row"> 
            <div class="white-box" >  
                <div class="row">
                    <div class="col-sm-8">
                        <h1><b>ใบส่งคืนสินค้า </h1>
                    </div>
                    <div class="col-sm-4 right"> 
                        <h3><b> อ้างอิงจาก<br/> INVOICE ID.</b>  <?= $r['invoice_id'] ?> </b> </h3>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-12">
                        <h4>ชื่อ.  <?= $rCustomerByIv['name'] ?> </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        โทร. <?= $rCustomerByIv['phone_number'] ?> 
                    </div>
                    <div class="col-sm-4">
                        ไลน์. <?= $rCustomerByIv['line_id'] ?> 
                    </div>
                    <div class="col-sm-4">
                        ที่อยู่. <?= $rCustomerByIv['address'] ?> 
                    </div>
                </div>
            </div>

        </div>  
        <div class="row"> 
            <div class="white-box">
                <div> 

                    <form name="returnProdcct" action="action/invoiceReturnProductSave.php" method="POST">
                        <input type="hidden" value="<?= $iv_id ?>" name="invoice_id">
                        <input type="hidden" value="<?= $r['invoice_id'] ?>" name="invoice_id_txt">

                        <table class="table table-bordered table-responsive" id='tbListOfInvoice'>
                            <thead>
                                <tr>
                                    <th style="width:2%">#</th>
                                    <th style="width:30%">สินค้า/Barcode</th>
                                    <th style="width:14%">รายละเอียดการคืน</th>
                                    <th style="width:8%">จำนวนที่สั่ง</th>
                                    <th style="width:8%">จำนวนที่คืน</th>
                                    <th style="width:10%">หน่วยคืน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sqlListIvProduct = "SELECT * FROM invoice_products WHERE invoice_id = $iv_id";
                                $rsListIvProduct = $conn->query($sqlListIvProduct);
                                $i = 1;
                                while ($rListIvProduct = $rsListIvProduct->fetch_assoc()) {
                                    $product_id = $rListIvProduct['product_id'];
                                    $barcode = $rListIvProduct['product_barcode'];
                                    $sqlProductName = "SELECT name from products where id = $product_id";
                                    $rsProduct = $conn->query($sqlProductName);
                                    $rProduct = $rsProduct->fetch_assoc();

                                    $sqlProReturn = "SELECT * from product_return where barcode = '$barcode' and invoice_id = '$iv_id'";
                                    $rsProRe = $conn->query($sqlProReturn);
                                    $rProRe = $rsProRe->fetch_assoc();


                                    $sqlpro = "SELECT 
                            p.id,
                            p.name as product_name,
                            p.barcode,
                            p.barcode_of_unit,
                            p.buy_price as buy_price,
                            p.sell_price as sell_price,
                            p.status,
                            pk.name as package_name,
                            u.name as unit_name
                            
                        FROM
                            products as p,
                            package as pk,
                            package_unit as pu,
                            unit as u
                        WHERE
                            p.package_unit_id = pu.id
                                AND pu.package_id = pk.id
                                AND pu.unit_id = u.id
                                AND p.status = 1
                                AND (p.barcode = $barcode OR p.barcode_of_unit = $barcode)";
                                    $rspro = $conn->query($sqlpro);
                                    $rpro = $rspro->fetch_assoc()
//                            echo $sqlpro;
                                    ?>
                                    <tr 
                                    <?php
                                    if ($rProRe['id']) {
                                        echo " style='background-color: #e4e4e4;color: black;'";
                                    }
                                    ?>
                                        >
                                        <td><?= $i ?></td>
                                        <td>
                                            <input type="hidden" name="barcode[]" value="<?= $barcode ?>">
                                            <input type="hidden" name="product_id[]" value="<?= $product_id ?>">
                                            <input type="hidden" name="return_product_id[]" value="<?= $rProRe['id'] ?>">
                                            <input type="hidden" name="manage_stock_id[]" value="<?= $rProRe['manage_stock_id'] ?>">
                                            <?= $rProduct['name'] . '[' . $barcode . ']' ?>
                                        </td>
                                        <td>
                                            <input type="text" name="detail[]" value="<?= $rProRe['detail'] ?>">
                                        </td>
                                        <td>
                                            <?= $rListIvProduct['value'] . ' ' . $rListIvProduct['unit'] ?>
                                            <?php
                                            if ($rpro['barcode'] === $barcode) {
                                                $sqlSelectSetting = "SELECT value_of_unit FROM product_setting WHERE product_id = $product_id";
                                                $rs2 = $conn->query($sqlSelectSetting);
                                                $r2 = $rs2->fetch_assoc();
                                                echo '<br/>(' . $r2['value_of_unit'] * $rListIvProduct['value'] . ' ' . $rpro['unit_name'] . ')';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <input type="number" name="value_return[]" value="<?= $rProRe['return_value'] ?>" style="text-align: right">
                                        </td>
                                        <td>
                                            <input type="hidden" name="unit[]" value="<?= $rpro['unit_name'] ?>">
                                            <?= $rpro['unit_name'] ?>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>

                        </table>

                        <div class="row">
                            <div class="col-sm-6">
                                <!--0= Awaiting 1 = Delivered, 2 = Paid ,3 = Paid but not deliver-->
                                <div class="left" style="text-align: left">
                                    <button onclick='printDiv();' type="button" class="btn btn-primary" ><span class="glyphicon glyphicon-print"></span> พิมพ์</button>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="right" style="text-align: right">

                                    <button id="save" type="submit" class="btn btn-primary">บันทึก</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//    function addValue(id, package_name, unit_name, value_of_unit) {
    function renewToStock() {
        $('#renewToStock').modal();
//        if (value_of_unit) {
//            $('#addToStock').modal();
//            $('#addToStock #value').focus();
//            $('#addToStock #barcode_id').val(id);
//            $('#addToStock #barcode_id_span').text(id);
//            $('#addToStock #package_name').text(package_name);
//            $('#addToStock #unit_name').text(unit_name);
//            $('#addToStock #value_of_unit').text(value_of_unit);
//            $('#addToStock #barcode-page_item').attr('action', 'action/stockAdd.php');
//        } else {
//            alert('Not have Value of Unit');
//        }
    }

    function runItem() {
        var $area = $('#tbListOfInvoice');
        $('tr[data-rol-product]', $area).each(function (i, d) {
            $(this).find('td:first').html(i + 1);
        });
    }
    $(document).ready(function () {

        function checkNumberInput(val) {
            return typeof val === 'number' ? true : false;
        }

        $('button#save').click(function () {
//            var $list = $('[data-rol-product]');
//            var productList = [];
//            var total = $('[name="total"]').val();
//            var note = $('[name="note"]').val();
//            if ($list.length) {
//                $('[data-rol-product]').each(function () {
//                    var $l = $(this);
//                    var product = {
//                        id: $l.find('[name="selectedproduct_id"]').val(),
//                        product_barcode: $l.find('[name="selected_barcode"]').val(),
//                        description: $l.find('[name="selected_description"]').val(),
//                        value: $l.find('[name="selected_value"]').val(),
//                        unit: $l.find('[name="selected_unit"]').val(),
//                        unit_price: $l.find('[name="selected_unit_price"]').val(),
//                        price: $l.find('[name="price"]').val()
//                    }
//                    productList.push(product);
//                });
//                var invoice = {
//                    customer: $('[name="customer"]').val(),
//                    date: $('[name="date"]').val(),
//                    term: $('[name="term"]').val(),
//                    duedate: $('[name="duedate"]').val(),
//                    productList: productList,
//                    note: note,
//                    total: total,
//                    id: <?= $iv_id ?>
//                };
//                console.log(invoice);
//                $.ajax({
//                    type: 'POST',
//                    data: invoice,
//                    url: 'action/invoiceEdit.php',
//                    success: function (data) {
//                        if (data == '1') {
//                            $.notify({
//                                message: 'Update this invoice successful, Please Wait...'
//                            }, {
//                                type: 'warning',
//                                delay: 200,
//                                showProgressbar: true,
//                                onClose: function () {
//                                    location.reload();
//                                }
//                            })
//                        } else {
//                            alert('Error');
//                        }
//                    }
//                });
//            }
        });
//        $('[name="productOfInvoice"]').submit(function () {
//            var $this = $(this);
//            console.log(objectifyForm($this))
//        });


        $('body').on('change', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('click', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('keyup', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('change', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }

        });
        $('body').on('click', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }
        });
        $('body').on('keyup', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }
        });
        $('body').on('click', '[data-btn="return"]', function () {
            $('#renewToStock').modal();
//            var dialog = '<div class="row"><div class="col-sm-12">sdsdsd</div></div>';
//            var $d = $(dialog).dialog({
//                title: 'สินค้าตีคืน'
//            });
//            $d.find('button').on('click', function () {
//                var $self = $(this);
//                $.each(typeofpd, function (i, d) {
//                    if (d.type === $self.data('type')) {
//                        $d.dialog('close');
//                        var unit_price = 0;
//                        $tr += '<tr data-rol-product>';
//                        $tr += '<td>' + (+$('[data-rol-product]').length + 1) + '</td>';
//                        $tr += '<td><input type="hidden" name="selected_barcode" value="' + d.barcode + '"> <input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
//                        $tr += '<td><input type="text" name="selected_description"></td>';
//                        $tr += '<td><input type="number" name="selected_value" value="1"></td>';
//                        $tr += '<td><input type="hidden" name="selected_unit" value="' + d.name + '" >';
//                        $tr += '' + d.name + '';
//                        $tr += '</td>';
//                        if (d.type == 'package') {
//                            unit_price = +r.product.sell_price_package;
//                        } else {
//                            unit_price = +r.product.sell_price;
//                        }
//
//                        $tr += '<td> <input type="hidden" name="selected_unit_price" value="' + unit_price + '" >' + unit_price + '</td>';
//                        $tr += '<td><input type="number" name="price" value="' + unit_price + '"></td>';
//                        $tr += '<td> <button class="glyphicon glyphicon-trash" data-btn="delete"></button><button class="glyphicon glyphicon-cloud-download" data-btn="return"></button></td>';
//                        $tr += '</tr>';
//                        $('#tbListOfInvoice tbody').append($($tr).data(productObject));
//                        calculate();
//                    }
//                });
//            });
        });
    });
    function printDiv()
    {
        var divToPrint = document.getElementById('DivIdToPrint');
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><head> <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="print"> <link href="css/jquery.bootgrid.min.css" rel="stylesheet" media="print"> </head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    }

</script>