 
<?php
include 'include/function.php';
if (!isset($_GET['id'])) {
    exit();
} else {
    $iv_id = $_GET['id'];
    $condition = isset($_GET['condition']) ? $_GET['condition'] : '';
    $month = isset($_GET['month']) ? $_GET['month'] : '';
    $year = isset($_GET['year']) ? $_GET['year'] : '';
}

//
//$ivCustomer = $r['customer_id'];
//$invoice_type = $r['invoice_type'];
//$invoice_due_date = $r['invoice_due_date'];

$sqlCustomerByIv = "SELECT * FROM customers WHERE id = $iv_id";
$rsCustomerByIv = $conn->query($sqlCustomerByIv);
$rCustomerByIv = $rsCustomerByIv->fetch_assoc();





//
//
//$sqlProduct = "select
//	p.id,
//	p.name,
//	p.sell_price,
//	p.sell_price_package,
//	pa.name as package_name,
//	u.name as unit_name
//from
//	products as p,
//	package_unit as pu,
//	unit as u,
//	package as pa
//where
//	p.package_unit_id = pu.id
//	and pu.package_id = pa.id
//	and pu.unit_id = u.id";
//
//$rsProduct = $conn->query($sqlProduct);
?>
<style>
    .white-box{
        margin-bottom: 10px;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <form>
            <input type="hidden" value="<?= $_GET['page'] ?>" name="page"/>
            <input type="hidden" value="<?= $iv_id ?>" name="id"/>
            เงื่อนไขการค้นหา: 
            <select name="condition" class="form-control-line">
                <option <?php
                if ($condition === 'all') {
                    echo "selected";
                }
                ?> value="all">ทั้งหมด</option>
                <option <?php
                if ($condition === 'month') {
                    echo "selected";
                }
                ?> value="month">ประจำเดือน</option>
                <option <?php
                if ($condition === 'year') {
                    echo "selected";
                }
                ?> value="year">ประจำปี</option>
            </select>
            <span data-role="month_area" class="hide">
                เลือกเดือน: 
                <select name="month" class="form-control-line">
                    <option <?php
                    if ($month == '1') {
                        echo "selected";
                    }
                    ?> value="1">มกราคม</option>
                    <option <?php
                    if ($month == '2') {
                        echo "selected";
                    }
                    ?> value="2">กุมภาพันธ์</option>
                    <option <?php
                    if ($month == '3') {
                        echo "selected";
                    }
                    ?> value="3"> มีนาคม</option>
                    <option <?php
                    if ($month == '4') {
                        echo "selected";
                    }
                    ?> value="4"> เมษายน</option>
                    <option <?php
                    if ($month == '5') {
                        echo "selected";
                    }
                    ?> value="5"> พฤษภาคม</option>
                    <option <?php
                    if ($month == '6') {
                        echo "selected";
                    }
                    ?> value="6"> มิถุนายน</option>
                    <option <?php
                    if ($month == '7') {
                        echo "selected";
                    }
                    ?> value="7"> กรกฎาคม</option>
                    <option <?php
                    if ($month == '8') {
                        echo "selected";
                    }
                    ?> value="8"> สิงหาคม</option>
                    <option <?php
                    if ($month == '9') {
                        echo "selected";
                    }
                    ?> value="9">กันยายน </option>
                    <option <?php
                    if ($month == '10') {
                        echo "selected";
                    }
                    ?> value="10"> ตุลาคม</option>
                    <option <?php
                    if ($month == '11') {
                        echo "selected";
                    }
                    ?> value="11">พฤศจิกายน </option>
                    <option <?php
                    if ($month == '12') {
                        echo "selected";
                    }
                    ?> value="12"> ธันวาคม</option>

                </select>
            </span>
            <span data-role="year_area" class="hide">
                เลือกปี 
                <select name="year" class="form-control-line">
                    <?php
                    $sqlGetYear = "select YEAR(invoice_date) as allyear from invoice GROUP BY YEAR(invoice_date) ORDER by invoice_date";
                    $rsGetYear = $conn->query($sqlGetYear);
                    while ($rGetYear = $rsGetYear->fetch_assoc()) {
                        if ($year == $rGetYear['allyear']) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option <?= $selected ?> value="<?= $rGetYear['allyear'] ?>"><?= $rGetYear['allyear'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </span>
            <button class="btn btn-sm" type="submit">ค้นหา</button>
        </form>

    </div>
    <div class="col-sm-12">
        <div class="white-box" id='DivIdToPrint'>  
            <div class="row">
                <div class="col-sm-12">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h2><b>รายงานสรุปยอดการสั่งซื้อ 
                                        <?php
                                        if ($condition == 'month') {
                                            echo "ประจำเดือน <span id='monthTxt'></span> ปี " . $year;
                                        }
                                        if ($condition == 'year') {
                                            echo "ประจำปี" . $year;
                                        }
                                        ?>

                                    </b></h2>

                            </td>

                        </tr>
                    </table>

                </div>

            </div> 
            <div class="row">
                <div class="col-sm-12"> 
                    <h4>ชื่อ.  <?= $rCustomerByIv['name'] ?> </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    โทร. <?= $rCustomerByIv['phone_number'] ?> 
                </div>
                <div class="col-sm-4">
                    ไลน์. <?= $rCustomerByIv['line_id'] ?> 
                </div>
                <div class="col-sm-4">
                    ที่อยู่. <?= $rCustomerByIv['address'] ?> 
                </div>
            </div>

            <div style="padding-top: 40px"> 
                <!--                <div style="background-color: #ecf0f4;
                                     padding: 6px;
                                     font-size: 16px;
                                     font-weight: bold;">
                                    BILL TO.
                                </div>-->
                <?php
                if (!$condition) {
                    ?>
                    <div class="panel panel-primary">
                        <div class="panel-body">กรุณาเลือกเงื่อนไขการค้นหา</div>
                    </div>
                    <?php
                } else {
                    ?>
                    <table class="table table-bordered table-responsive" id='tbListOfInvoice'>
                        <thead>
                            <tr style="background-color: #ecf0f4;font-weight: bold">
                                <th style="width:2%">#</th>
                                <th style="width:30%">เลขที่ใบสั่งซื้อ</th>
                                <th style="width:14%">วันที่สั่งซื้อ</th>
                                <th style="width:8%">ราคารวม</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "SELECT id, invoice_id, customer_id, invoice_date, invoice_payment_status, 
    invoice_type, invoice_credit, invoice_due_date, note, date_inserted, 
    date_last_update, userId FROM invoice WHERE customer_id = $iv_id ";


                            if ($condition == 'year') {
                                $sql .= " AND YEAR(invoice_date) = '$year'";
                            } else if ($condition == 'month') {
                                $sql .= " AND MONTH(invoice_date) = '$month' AND YEAR(invoice_date) = '$year'";
                            }


                            $rs = $conn->query($sql);
                            $i = 1;
                            $total = 0;
                            while ($r = $rs->fetch_assoc()) {
                                $invoice_id = $r['id'];
                                $sqlIV = "SELECT sum(price) as sumprice from invoice_products where invoice_id = $invoice_id";

                                $rsIV = $conn->query($sqlIV);
                                $rIV = $rsIV->fetch_assoc();
                                ?>
                                <tr data-rol-product="">
                                    <td><?= $i ?></td>
                                    <td>
                                        <?= $r['invoice_id'] ?>
                                    </td>
                                    <td>
                                        <?= date('d/m/Y', strtotime($r['invoice_date'])) ?>
                                    </td>
                                    <td>
                                        <?= number_format($rIV['sumprice']) ?>
                                    </td> 

                                </tr>
                                <?php
                                $total = $total + (int) $rIV['sumprice'];
                                $i++;
                            }
                            ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="1"></td>

                                <td colspan="3" style="text-align: right">
                                    <span >
                                        <?= number_format($total, 2) ?> บาท
                                    </span>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php
                }
                ?>

            </div>
        </div> 
        <div class="row">
            <div class="col-sm-6">
                <div class="left" style="text-align: right">
                    <button onclick='printDiv();' id="Print" type="button" class="btn btn-danger">Print</button>
                </div>
            </div> 
        </div>
    </div>
    <script>
        function hideShowCondition(val) {
            if (val == 'month') {
                $('[data-role="month_area"]').removeClass('hide')
                $('[data-role="year_area"]').removeClass('hide')
                $('#monthTxt').text($('[name="month"] option:selected').text())
            } else if (val == 'year') {
                $('[data-role="month_area"]').addClass('hide')
                $('[data-role="year_area"]').removeClass('hide')
                $('#monthTxt').text($('[name="month"] option:selected').text())
            } else {
                $('[data-role="month_area"]').addClass('hide')
                $('[data-role="year_area"]').addClass('hide')
            }
        }
        $(document).ready(function () {
            hideShowCondition($('[name="condition"]').val())
            $('[name="condition"]').change(function () {
                var val = $(this).val();
                hideShowCondition(val)
            })
        });
        function printDiv()
        {
            var divToPrint = document.getElementById('DivIdToPrint');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><head> <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="print"> <link href="css/jquery.bootgrid.min.css" rel="stylesheet" media="print"><link href="css/print.css" rel="stylesheet" media="print"> </head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 10);
        }

    </script>