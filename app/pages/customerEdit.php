<?php
include 'include/function.php';
$id = $_GET['id'];
$sql = "SELECT id,name,phone_number,line_id,address "
        . "FROM customers WHERE id = $id";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form autocomplete="off" action="action/customerEdit.php" method="post" class="form-horizontal form-material">
                <input type="hidden" name='id' value="<?= $r['id'] ?>" />  
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?= $r['name'] ?>">
                    </div>
                    <div class="col-sm-6">
                        <label for="name">phone_number:</label>
                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="<?= $r['phone_number'] ?>" >
                    </div>               
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="name">line id:</label>
                        <input type="text" class="form-control" id="line_id" name="line_id" value="<?= $r['line_id'] ?>">
                    </div>
                    <div class="col-sm-6">
                        <label for="name">address:</label>
                        <input type="text" class="form-control" id="address" name="address" value="<?= $r['address'] ?>">
                    </div>      
                </div>
                <div class="row"> 
                    <div class="col-xs-6">
                        <a onclick="confirmDelete(<?= $id ?>)" type="button" class="btn btn-danger">Delete</a>
                    </div>
                    <div class="col-xs-6"> 
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                        <a onclick="history.back();" class="btn btn-default pull-right" style="margin-right: 10px">Cancel</a>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/factoryDelete.php');
    }
    $(document).ready(function () {

    });
</script>
