<?php
$sql = "SELECT id,name,buy_price,sell_price, barcode, date_inserted FROM products";
$rs = $conn->query($sql);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="text-right btn-addnew">
            <a href="" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>
                Add New
            </a>
        </div>
        <div class="white-box">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Barcode</th>
                            <th>Product Name</th>
                            <th>Package</th>
                            <th>unit</th>
                            <th>Buy</th>
                            <th>Sell</th>
                            <th>Remain Stock</th> 
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($r = $rs->fetch_assoc()) {
                            $i++;
                            ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $r['barcode'] ?></td>
                                <td><?= $r['name'] ?></td>
                                <td></td>
                                <td></td>
                                <td><?= $r['buy_price'] ?></td>
                                <td><?= $r['sell_price'] ?></td>
                                <td> - </td> 
                                <td>
                                    <button type="button" class="btn btn-info btn-lg btn-sm" data-toggle="modal" data-target="#myModal<?=$i?>">Detail</button>
                                    <div class="modal fade" id="myModal<?=$i?>" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"><?= $r['name'] ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p><form autocomplete="off" action="action/productEdit.php" method="post">
                                                        <input type="hidden" name='id' value="<?= $id ?>" />
                                                        <div class="form-group">
                                                            <label for="barcode">Barcode:</label>
                                                            <input type="text" class="form-control" id="barcode" name="barcode" value="<?= $r['barcode'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="name">Name:</label>
                                                            <input type="text" class="form-control" id="name" name="name" value="<?= $r['name'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="package">Package:</label>
                                                            <input type="text" class="form-control" id="package" name="package" value="<?= $r['package_id'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="unit">Unit:</label>
                                                            <input type="text" class="form-control" id="unit" name="unit" value="#">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="buy">Buy Price:</label>
                                                            <input type="text" class="form-control" id="buy" name="buy" value="<?= $r['buy_price'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sell">Sell Price:</label>
                                                            <input type="text" class="form-control" id="sell" name="sell" value="<?= $r['sell_price'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="remain">Remaining Stock:</label>
                                                            <input type="text" class="form-control" id="remain" name="remain" value="#">
                                                        </div>
                                                    </form></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                                <a title="Edit" class="btn btn-info btn-sm" href="?page=productsEdit&id=<?= $r[id] ?>">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </a>
                                                <a title="Remove" class="btn btn-danger btn-sm" href="#">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                </a>                   
                                            </div>
                                        </div>


                                </td>
                            </tr>
                            <?php
                        }
                        ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>