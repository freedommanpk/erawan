<?php ?>
<div class="row">
    <div class="col-sm-12"> 
        <div class="white-box">
            <div class="table-responsive">
                <table id="grid-data-api" class="table table-hover" data-toggle="bootgrid" data-ajax="true" data-url="./api/getcustomerReport.php">
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>
                            <th data-column-id="customers_name">Name</th>
                            <th data-column-id="phone_number">PhoneNumber</th>
                            <th data-column-id="line_id">Line Id</th>     
                            <th data-column-id="action">View & Print</th>
                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
    </div>
</div>
 