<?php
include 'include/function.php';
$id = $_GET['id'];
$sql = "SELECT name,code,manufacturer,tel,line_id,house_number,road,district,sub_district,moo,"
        . "province,country,zip_code "
        . "FROM factory WHERE id = $id";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();

$sqlsale = "SELECT name, phone FROM  factorie_sales where factory_id = '$id'";
$rssale = $conn->query($sqlsale);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form autocomplete="off" action="action/factoryEdit.php" method="post" class="form-horizontal form-material">
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-xs-12"> 
                            <h3>Factory Detail</h3>
                        </div>
                        <div class="col-sm-3">
                            <label for="name">Code:</label>
                            <?= $r['code'] ?> 
                        </div>
                        <div class="col-sm-3">
                            <label for="name">Name:</label>
                            <?= $r['name'] ?> 
                        </div>
                        <div class="col-sm-6">
                            <label for="Manufacturer">Manufacturer:</label>
                            <?= $r['manufacturer'] ?>
                        </div>     
                    </div>
                    <div class="row"> 
                        <div class="col-sm-3">
                            <label for="Tel">Tel:</label>
                            <?= $r['tel'] ?>
                        </div>
                        <div class="col-sm-3">
                            <label for="Line_ID">Line ID:</label>
                            <?= $r['line_id'] ?>
                        </div>      

                        <div class="col-sm-3"> 
                            <label for="Housenumber">House number:</label>  
                            <?= $r['house_number'] ?>
                        </div>
                        <div class="col-sm-3"> 
                            <label for="Road">Road:</label>  
                            <?= $r['road'] ?>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-sm-3"> 
                            <label for="District">District:</label>
                            <?= $r['district'] ?>
                        </div>
                        <div class="col-sm-3"> 
                            <label for="SubDistrict">Sub District:</label>  
                            <?= $r['sub_district'] ?>
                        </div> 


                        <div class="col-sm-3"> 
                            <label for="Moo">Moo:</label> 
                            <?= $r['moo'] ?>
                        </div>
                        <div class="col-sm-3"> 
                            <label for="Province">Province:</label>
                            <?= $r['province'] ?>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-sm-6">
                            <label for="Country">Country:</label>
                            <?= $r['country'] ?>
                        </div>
                        <div class="col-sm-6">
                            <label for="ZipCode">Zip Code:</label>
                            <?= $r['zip_code'] ?>
                        </div>
                    </div>

                    <div class="row"> 
                        <div class="col-xs-12"> 
                            <h3>Factory Sales Executive</h3>
                        </div>
                        <?php
                        while ($rsale = $rssale->fetch_assoc()) {
                            ?>
                            <div class="col-xs-6"> 
                                <label for="sale_name_ex1">Name:</label>
                                <?= $rsale['name']; ?>
                            </div>
                            <div class="col-xs-6"> 
                                <label for="sale_phone_ex1">Telephone:</label>
                                <?= $rsale['phone'] ?>
                            </div>
                            <?php
                        }
                        ?>


                    </div>

                </div> 
        </div>  
        <div class="row"> 
            <div class="col-xs-12">  
                <a onclick="history.back();" class="btn btn-default pull-right" style="margin-right: 10px">Cancel</a>
            </div>
        </div>
        </form> 
    </div>
</div>
</div>
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/productDelete.php');
    }
    $(document).ready(function () {
        function fetchUnit(id) {
            $.ajax({url: "api/getunit.php?package_id=" + id + "", success: function (result) {
                    $('#unit').empty();
                    $('#unitlist').empty();
                    var dfUnit_id = 0;
                    if ($('#unit_id').val()) {
                        dfUnit_id = $('#unit_id').val();
                    }
                    var data = JSON.parse(result);
                    data.forEach(function (v) {
//                        var html = "<div class='checkbox'> <label><input type='checkbox' value='" + v.id + "'>" + v.name + "</label></div>";
                        if (+dfUnit_id === +v.id) {
                            $('#unit').append("<option selected value='" + v.id + "'>" + v.name + "</option>");
//                            $('#unitlist').append(html);
                            $('#unit_display_name').html(v.name);
                        } else {
//                            $('#unitlist').append(html);
                            $('#unit').append("<option value='" + v.id + "'>" + v.name + "</option>");
                        }
                    });
                }
            });
        }
        if ($('#packages').val()) {
            fetchUnit($('#packages').val());
        }
        $('#packages').change(function () {
            var id = $(this).val();
            fetchUnit(id);
        });
        $('#unit').change(function () {
            $('#unit_display_name').html($("#unit option:selected").text());
        });
    });
</script>
