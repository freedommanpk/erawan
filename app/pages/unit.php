<?php
$package_id = $_REQUEST['packages_id'];

$sql1 = "SELECT id,name FROM package WHERE status = 1 ORDER BY name asc";
$rs1 = $conn->query($sql1);

if ($package_id) {
    $sqlGetUnit = "SELECT id,name FROM unit order by name";
    $rs2 = $conn->query($sqlGetUnit);

    $sqlGetPackageUnit = "SELECT id, unit_id FROM package_unit WHERE package_id = $package_id AND status = 1";
    $rs3 = $conn->query($sqlGetPackageUnit);

    $unitExist = array();
    $puExist = array();
    while ($r3 = $rs3->fetch_assoc()) {
        $unitExist[] = $r3['unit_id'];
        $puExist[] = $r3['id'];
    }
}
?>
<div class="row package_page">
    <div class="col-sm-12"> 
        <div class="text-right btn-addnew">
            <a href="?page=unitAdd" class="btn btn-primary"> 
                Add New/Delete
            </a>
        </div>
        <div class="white-box"> 
            <div > 
                <form class="" action="">
                    <input type="hidden" name="page" value="unit"/>
                    <div class="form-group">
                        <div class="row"> 
                            <label for="package">Package:</label>  
                            <select class="form-control " id="packages"  name="packages_id" onchange="this.form.submit()" >
                                <option value="">--- Select ---</option>
                                <?php
                                while ($r1 = $rs1->fetch_assoc()) {
                                    if ($r1['id'] == $package_id) {
                                        $selection = 'selected';
                                    } else {
                                        $selection = '';
                                    }
                                    ?>
                                    <option <?= $selection ?> value="<?= $r1['id'] ?>"><?= $r1['name'] ?></option>
                                    <?php
                                }
                                ?>
                            </select> 
                        </div>
                    </div>
                </form>
            </div>

            <div>  
                <?php
                if ($rs2) {
                    ?>
                    <form action="action/packageUnitAdd.php" method="POST">
                        <input type="hidden" name="package_id" value="<?= $package_id ?>"/>
                        <div class="form-group">
                            <div class="row"> 
                                <?php
                                while ($r2 = $rs2->fetch_assoc()) {
                                    $checked = '';
                                    $data_having = FALSE;
                                    if (in_array($r2['id'], $unitExist)) {
                                        $checked = 'checked';
                                        $data_having = TRUE;
                                    }
                                    ?> 
                                    <div>
                                        <label>
                                            <input <?= $checked ?> name="units[]" data-having="<?= $data_having ?>" type="checkbox" value="<?= $r2['id'] ?>"><?= $r2['name'] ?>
                                            <input name="actions[]" type="hidden" />
                                            <input name="unit_ids[]" type="hidden" value="<?= $r2['id'] ?>" />
                                        </label>
                                    </div> 
                                    <?php
                                }
                                ?> 
                            </div>
                        </div>
                        <div class="row">  
                            <div class="col-xs-12"> 
                                <button type="submit" class="btn btn-primary pull-right">Save</button>
                            </div>
                        </div>
                    </form>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-danger" role="alert">
                        <strong>Oh! </strong> Please select package 
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('[name="units[]"]').change(function () {

            if ($(this).is(':checked')) {
                if ($(this).data('having')) {
                    $(this).next().val('');
                } else {
                    $(this).next().val('insert');
                }
            } else {
                if ($(this).data('having')) {
                    $(this).next().val('delete');
                } else {
                    $(this).next().val('');
                }
            }
        });
    });
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/packageDelete.php');
    }

</script>