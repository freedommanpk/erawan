<?php
include 'include/function.php';

$sql = "SELECT id, name,phone_number,line_id,address FROM customers";
$rs = $conn->query($sql);

$sqlProduct = "select
	p.id,
	p.name,
	p.sell_price,
	p.sell_price_package,
	pa.name as package_name,
	u.name as unit_name
from
	products as p,
	package_unit as pu,
	unit as u,
	package as pa
where
	p.package_unit_id = pu.id
	and pu.package_id = pa.id
	and pu.unit_id = u.id
        and p.status = 1";

$rsProduct = $conn->query($sqlProduct);
?>
<style>
    .white-box{
        margin-bottom: 10px;
    }
</style>
<div class="row">
    <form name="invoiceDetail" onsubmit="return false">
        <div class="col-sm-12">
            <div class="white-box"> 
                <label for="factory">Customer:</label>
                <select class="form-control" id="customer"  name="customer" required="" >
                    <option value="">--- Select ---</option>
                    <?php
                    while ($r = $rs->fetch_assoc()) {
                        $address = 'LineID: ' . $r['line_id'] . '&#013;Address:' . $r['address'];
//                        if ($r['factory_id'] == $r3['id']) {
//                            $selection = 'selected';
//                        } else {
                        $selection = '';
//                        }
                        ?>
                        <option <?= $selection ?> title="<?= $address ?>" value="<?= $r['id'] ?>"><?= $r['name'] ?> (<?= $r['phone_number'] ?>)</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div> 
        <div class="devider"></div>
        <div class="col-sm-12">
            <div class="white-box">
                <div> 
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="date">Date</label>
                            <input type="text" name="date" id="date" class="form-control" data-role="datepicker" autocomplete="off" value="<?= date("d/m/Y") ?>">
                        </div>
                        <div class="col-sm-4">
                            <label for="date">Invoice Type</label>
                            <select class="form-control" id="term"  name="term" >
                                <option value="Cash"> Cash </option>
                                <option value="Transfer"> Transfer </option> 
                                <option value="Cheque"> Cheque </option> 
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label for="date">Due date</label>
                            <input type="text" name="duedate" class="form-control" data-role="datepicker" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </form>
    <div class="col-sm-12">
        <div class="white-box">
            <div> 
                <div class="row">
                    <form name="search_product" onsubmit="return false;">
                        <div class="col-sm-2">
                            <select class="form-control" id="type_of_search"  name="type_of_search" >
                                <option value="1"> Barcode </option>
                                <option value="2"> Search </option> 
                            </select>
                        </div>
                        <div class="col-sm-10">
                            <div id="showBarcode">
                                <input type="number" data-toggle="tooltip" data-placement="top" autocomplete="off" 
                                       title="Please Key Barcode or Scan Barcode and presss 'Enter' to Search the Product" 
                                       class="form-control barcode-input barcode-input-for-add-iv" id="barcode" name="barcode" value="">
                            </div>
                            <div id="showListing" class="hide">
                                <select class="form-control" id="product"  name="product" >
                                    <option value="">--- Select ---</option>
                                    <?php
                                    while ($rProduct = $rsProduct->fetch_assoc()) {
                                        ?>
                                        <option value="<?= $rProduct['id'] ?>" data-id="<?= $rProduct['id'] ?>" data-name="<?= $rProduct['name'] ?>" data-sell-price="<?= $rProduct['sell_price'] ?>" data-sell-price-package="<?= $rProduct['sell_price_package'] ?>" data-package-name="<?= $rProduct['package_name'] ?>" data-unit-name="<?= $rProduct['unit_name'] ?>"><?= $rProduct['name'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <form name="productOfInvoice" onsubmit="return false">
                    <table class="table table-bordered table-responsive" id='tbListOfInvoice'>
                        <thead>
                            <tr>
                                <th style="width:2%">#</th>
                                <th style="width:30%">สินค้า</th>
                                <th style="width:14%">รายละเอียด</th>
                                <th style="width:8%">จำนวน</th>
                                <th style="width:10%">หน่วย</th>
                                <th style="width:10%">ราคาต่อหน่วย</th>
                                <th style="width:10%">รวมเป็นเงิน</th>
                                <th style="width:1%"></th>
                            </tr>
                        </thead>
                        <tbody>
    <!--                        <tr>
                                <td>1</td>
                                <td>ยางพารากกกกน</td>
                                <td><input type="text" name="description"></td>
                                <td><input type="number" name="value" value="2"></td>
                                <td>
                                    <select>
                                        <option>-- Select --</option>
                                        <option>ลัง</option>
                                        <option>แถว</option>
                                    </select>
                                </td>
                                <td>60</td>
                                <td>120</td>
                                <td><button>X</button></td>
                            </tr>-->
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="5"><textarea class="form-control" name="note" placeholder="หมายเหตุ/บันทึก/เพิ่มเติม"></textarea></td>
                                <td colspan="2"><input type="hidden" name="total" /><span data-name="total"></span> บาท</td>
                            </tr>
                        </tfoot>
                    </table>
                </form>
                <div class="right" style="text-align: right">
                    <button id="save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function calculate() {
        var total = 0;
        var $area = $('#tbListOfInvoice');
        $('[name="price"]', $area).each(function (i, d) {
            var price = parseInt($(this).val());
            total = total + price;
        });
        $('[name="total"]').val(total);
        $('[data-name="total"]').text(total);
    }
    function runItem() {
        var $area = $('#tbListOfInvoice');
        $('tr[data-rol-product]', $area).each(function (i, d) {
            $(this).find('td:first').html(i + 1);
        });
    }
    $(document).ready(function () {
        function checkNumberInput(val) {
            return typeof val === 'number' ? true : false;
        }
        function calculatewithvalue($self) {
            var $tr = $self.closest('tr');
            var value = parseInt($self.val());
            var priceofunit = parseInt($self.closest('td').next().next().text())
            $tr.find('[name="price"]').val(priceofunit * value);
            calculate();
        }

        $('button#save').click(function () {
            var $list = $('[data-rol-product]');
            var productList = [];
            var total = $('[name="total"]').val();
            var note = $('[name="note"]').val();
            if ($list.length) {
                $('[data-rol-product]').each(function () {
                    var $l = $(this);
                    var product = {
                        id: $l.find('[name="selectedproduct_id"]').val(),
                        product_barcode: $l.find('[name="selected_barcode"]').val(),
                        description: $l.find('[name="selected_description"]').val(),
                        value: $l.find('[name="selected_value"]').val(),
                        unit: $l.find('[name="selected_unit"]').val(),
                        unit_price: $l.find('[name="selected_unit_price"]').val(),
                        price: $l.find('[name="price"]').val()
                    }
                    productList.push(product);
                });
                var invoice = {
                    customer: $('[name="customer"]').val(),
                    date: $('[name="date"]').val(),
                    term: $('[name="term"]').val(),
                    duedate: $('[name="duedate"]').val(),
                    productList: productList,
                    note: note,
                    total: total
                }

                $.ajax({
                    type: 'POST',
                    data: invoice,
                    url: 'action/invoiceAdd.php',
                    success: function (data) {
                        if (data == '1') {
                            $.notify({
                                message: 'Add done'
                            }, {
                                type: 'info',
                                delay: 200,
                                showProgressbar: true,
                                onClose: function () {
                                    location.reload();
                                }
                            })
                        } else {
                            alert('Error');
                        }
                    }
                });
            }
        });

//        $('[name="productOfInvoice"]').submit(function () {
//            var $this = $(this);
//            console.log(objectifyForm($this))
//        });


        $('body').on('change', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('click', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });
        $('body').on('keyup', '[name="selected_value"]', function () {
            calculatewithvalue($(this));
        });

        $('body').on('change', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }

        });
        $('body').on('click', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }
        });
        $('body').on('keyup', '[name="price"]', function () {
            if (checkNumberInput(+$(this).val())) {
                calculate();
            } else {
                $(this).val('0')
            }
        });

        $('body').on('click', '[data-btn="delete"]', function () {
            $(this).closest('tr').remove();
            calculate();
            runItem();
        })

        $('#customer').select2();
        $('#type_of_search').change(function () {
            var value = $(this).val();
            if (+value === 2) {
                $('#showBarcode').addClass('hide');
                $('#showListing').removeClass('hide');
                if (!$('#product').hasClass('select2-hidden-accessible')) {
                    setTimeout(function () {
                        $('#product').select2();
                    }, 100)
                }

            } else {
                $('#showBarcode').removeClass('hide');
                $('#showListing').addClass('hide');
            }
        });

        $('#product').on("select2:select", function (e) {
            var data = $(this).find(':selected').data();

        });

        $('form[name="search_product"]').submit(function () {
            var data = objectifyForm($(this));
            $.ajax({
                type: 'GET',
                data: data,
                dataType: 'json',
                url: 'api/getProductToList.invoiceAdd.php',
                success: function (r) {
                    var productObject = {
                        id: r.product.id,
                        type: r.type_of_product,
                        typeid: r.type_of_product_id
                    };
                    var $tr = '';

                    if (r.type_of_search == 1) {
                        var unit_price = 0;
                        $tr += '<tr data-rol-product>';
                        $tr += '<td>' + (+$('[data-rol-product]').length + 1) + '</td>';
                        $tr += '<td><input type="hidden" name="selected_barcode" value="' + data.barcode + '"> <input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
//                        $tr += '<td><input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
                        $tr += '<td><input type="text" name="selected_description"></td>';
                        $tr += '<td><input type="number" name="selected_value" value="1"></td>';
                        $tr += '<td><input type="hidden" name="selected_unit" value="' + r.type_of_product_name + '" >';
                        $tr += '' + r.type_of_product_name + '';
                        $tr += '</td>';

                        if (r.type_of_product == 'package') {
                            unit_price = +r.product.sell_price_package;
                        } else {
                            unit_price = +r.product.sell_price;
                        }

                        $tr += '<td><input type="hidden" name="selected_unit_price" value="' + unit_price + '" >' + unit_price + '</td>';
                        $tr += '<td><input type="number" name="price" value="' + unit_price + '"></td>';
                        $tr += '<td><button class="btn btn-sm" data-btn="delete">X</button></td>';
                        $tr += '</tr>';

                        $('#tbListOfInvoice tbody').append($($tr).data(productObject));
                        calculate();
                    } else {
                        var typeofpd = r.type_of_product_object;
                        var _package = typeofpd[0];
                        var _unit = typeofpd[1];
                        var bt1 = '<button style="width: 100%;font-size:25px;" class="btn btn-primary btn-lg" data-type=' + _package.type + '>' + _package.name + '</button>';
                        var bt2 = '<button style="width: 100%;font-size:25px;" class="btn btn-primary btn-lg" data-type=' + _unit.type + '> ' + _unit.name + '</button>';
                        var dialog = '<div class="row"><div class="col-sm-6">' + bt1 + '</div><div class="col-sm-6">' + bt2 + '</div></div>';
                        var $d = $(dialog).dialog({
                            title: 'Please choose unit of product'
                        });

                        $d.find('button').on('click', function () {
                            var $self = $(this);
                            $.each(typeofpd, function (i, d) {
                                if (d.type === $self.data('type')) {
                                    $d.dialog('close');
                                    var unit_price = 0;
                                    $tr += '<tr data-rol-product>';
                                    $tr += '<td>' + (+$('[data-rol-product]').length + 1) + '</td>';
                                    $tr += '<td><input type="hidden" name="selected_barcode" value="' + d.barcode + '"> <input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
//                                    $tr += '<td><input type="hidden" name="selectedproduct_id" value="' + r.product.id + '">' + r.product.product_name + '</td>';
                                    $tr += '<td><input type="text" name="selected_description"></td>';
                                    $tr += '<td><input type="number" name="selected_value" value="1"></td>';
                                    $tr += '<td><input type="hidden" name="selected_unit" value="' + d.name + '" >';
                                    $tr += '' + d.name + '';
                                    $tr += '</td>';

                                    if (d.type == 'package') {
                                        unit_price = +r.product.sell_price_package;
                                    } else {
                                        unit_price = +r.product.sell_price;
                                    }

                                    $tr += '<td> <input type="hidden" name="selected_unit_price" value="' + unit_price + '" >' + unit_price + '</td>';
                                    $tr += '<td><input type="number" name="price" value="' + unit_price + '"></td>';
                                    $tr += '<td><button class="btn btn-sm" data-btn="delete">X</button></td>';
                                    $tr += '</tr>';

                                    $('#tbListOfInvoice tbody').append($($tr).data(productObject));
                                    calculate();
                                }
                            });
                        })
                    }
//                    console.log(data);
//                    if (data) {
//                        $.notify({
//                            message: data
//                        }, {
//                            type: 'info',
//                            delay: 200,
//                            showProgressbar: true
//                        })
//                    }
                    $('[name="barcode"]').val('');

                }

            });
//            var data = '';
//            if (values.type_of_search == 1) {
//                data = values.barcode;
//            } else if (values.type_of_search == 2) {
//                data = values.product;
//            }
        });

        $('#product').change(function () {
            $('form[name="search_product"]').trigger('submit');
        });

        $('#type_of_search').change(function () {
            $('#barcode').val('');
            $('#product').prop("selectedIndex", 0).trigger('change.select2');
        });

    });
</script>