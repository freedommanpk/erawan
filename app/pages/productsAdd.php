<?php
include 'include/function.php';

$sql1 = "SELECT id,name FROM package WHERE status = 1 ORDER BY name asc";
$rs1 = $conn->query($sql1);

$sql2 = "SELECT package_id, unit_id FROM package_unit WHERE status = 1";
$rs2 = $conn->query($sql2);
$r2 = $rs2->fetch_assoc();

$sql3 = "SELECT id, name,manufacturer FROM factory";
$rs3 = $conn->query($sql3);

$sql4 = "SELECT id, name,code FROM category";
$rs4 = $conn->query($sql4);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form id="formProductAdd" autocomplete="off" action="action/productAdd.php" method="post" class="form-horizontal form-material" onsubmit="return false">
                <input type="hidden" name='id' value="<?= $id ?>" />
                <input type="hidden" name="unit_id" id="unit_id" "/>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" required="">
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="factory">Factory:</label>
                        <select class="form-control" id="factory"  name="factory"  required>
                            <option value="">--- Select ---</option>
                            <?php
                            while ($r3 = $rs3->fetch_assoc()) {
                                if ($r['factory_id'] == $r3['id']) {
                                    $selection = 'selected';
                                } else {
                                    $selection = '';
                                }
                                ?>
                                <option <?= $selection ?> value="<?= $r3['id'] ?>"><?= $r3['name'] ?> (<?= $r3['manufacturer'] ?>)</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Category:</label> 
                        <select class="form-control" id="category"  name="category"  required>
                            <option value="">--- Select ---</option>
                            <?php
                            while ($r4 = $rs4->fetch_assoc()) {
                                if ($r['category_id'] == $r4['id']) {
                                    $selection = 'selected';
                                } else {
                                    $selection = '';
                                }
                                ?>
                                <option <?= $selection ?> value="<?= $r4['id'] ?>"><?= $r4['code'] . ' ' . $r4['name'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6"> 
                            <label for="package">Package:</label>  
                            <select class="form-control " id="packages"  name="packages" required >
                                <option value="">--- Select ---</option>
                                <?php
                                while ($r1 = $rs1->fetch_assoc()) {
                                    if ($r1['id'] == $r2['package_id']) {
                                        $selection = 'selected';
                                    } else {
                                        $selection = '';
                                    }
                                    ?>
                                    <option <?= $selection ?> value="<?= $r1['id'] ?>"><?= $r1['name'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-6"> 
                            <label for="barcode">Barcode of Package:</label>
                            <input type="text" disabled=""   class="form-control " id="barcode" name="barcode_of_package">
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6"> 
                            <label for="unit">Unit:</label> 
                            <select class="form-control" id="unit"  name="unit" >  </select> 
                        </div>
                        <div class="col-sm-6"> 
                            <label for="barcode_of_unit">Barcode of Unit:</label> 
                            <input type="text" disabled class="form-control" id="barcode_of_unit" name="barcode_of_unit" >
                        </div>
                    </div> 
                </div> 
                <div class="form-group">
                    <label for="buy">Buy Price:</label>
                    <input type="text" class="form-control" id="buy" name="buy_price"  />
                </div>
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6"> 
                            <label for="sell">Sell Price Unit:</label>
                            <input type="text" class="form-control" id="sell" name="sell_price" />
                        </div>
                        <div class="col-sm-6"> 
                            <label for="sell">Sell Price Package:</label>
                            <input type="text" class="form-control" id="sell_package" name="sell_package" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6">
                            <label for="remain">Reorder Point: <span id="package_name"></span> </label>
                            <input type="text" class="form-control" id="reorder_point" name="reorder_point" value="<?= $reorder_point ?>">
                        </div> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">  
                        <div class="col-xs-12"> 
                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                            <a onclick="history.back();" class="btn btn-default pull-right" style="margin-right: 10px">Cancel</a>
                        </div>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/productDelete.php');
    }
    $(document).ready(function () {
        $('#category,#factory').select2( )
        $('#formProductAdd').submit(function () {
            var data = objectifyForm($(this));
            $.ajax({
                type: 'POST',
                data: data,
                url: 'action/productAdd.php',
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status == 'success') {
                        $.notify({
                            message: 'Add done'
                        }, {
                            type: 'info',
                            delay: 400,
                            showProgressbar: true
                        })
                        $('#barcode').val(obj.barcode_of_package);
                        $('#barcode_of_unit').val(obj.barcode_of_unit);

                    } else if (obj.status == 'fail') {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'Error'
                        }, {
                            type: 'danger',
                            delay: 400,
                            showProgressbar: true
                        })
                    } else if (obj.status == 'dup') {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'There cannot be same barcode'
                        }, {
                            type: 'danger',
                            delay: 400,
                            showProgressbar: true
                        })
                    } else if (obj.status == 'barcode_max') {
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'Barcode Not Generate!!!'
                        }, {
                            type: 'danger',
                            delay: 400,
                            showProgressbar: true
                        })
                    }
                }
            });
        })


        function fetchUnit(id) {
            $.ajax({url: "api/getunit.php?package_id=" + id + "", success: function (result) {
                    $('#unit').empty();
                    $('#unitlist').empty();
                    var dfUnit_id = 0;
                    if ($('#unit_id').val()) {
                        dfUnit_id = $('#unit_id').val();
                    }
                    var data = JSON.parse(result);
                    data.forEach(function (v) {
//                        var html = "<div class='checkbox'> <label><input type='checkbox' value='" + v.id + "'>" + v.name + "</label></div>";
                        if (+dfUnit_id === +v.id) {
                            $('#unit').append("<option selected value='" + v.id + "'>" + v.name + "</option>");
//                            $('#unitlist').append(html);
                            $('#unit_display_name').html(v.name);
                        } else {
//                            $('#unitlist').append(html);
                            $('#unit').append("<option value='" + v.id + "'>" + v.name + "</option>");
                        }
                    });
                }
            });
        }
        if ($('#packages').val()) {
            fetchUnit($('#packages').val());
        }
        $('#packages').change(function () {
            var id = $(this).val();
            fetchUnit(id);

            $('#package_name').text($(this).find('option:selected').text())
        });

        $('#unit').change(function () {
            $('#unit_display_name').html($("#unit option:selected").text());
        });
    });
</script>
