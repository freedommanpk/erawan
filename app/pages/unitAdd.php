<?php
include 'include/function.php';
$sql = "SELECT name,package_id,unit.id as id
FROM unit
LEFT JOIN package_unit ON unit.id = package_unit.unit_id AND package_unit.status = 1
GROUP BY name ORDER BY unit.id DESC";
$rs = $conn->query($sql);
?>
<div class="row package_page">
    <div class="col-sm-12">
        <div class="white-box">
            <form class="form-inline add-new-inline" action="action/unitAdd.php" method="post">
                <div class="form-group">
                    <label for="add_new_package">Add New : </label>
                    <input class="form-control" type="text" id="add_new_package" name="value"/>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
            <div style="margin-top: 80px;"> 
                <table class="table table-responsive table-bordered">
                    <tbody>
                        <?php
                        while ($r = $rs->fetch_assoc()) {
                            $id = $r['id'];
                            echo "<tr>";
                            echo "<td>" . $r['name'];
                            if (!$r['package_id']) {
                                echo "<button onclick='confirmDelete($id)' title='Delete' class='btn btn-danger btn-sm btn-2percent'>
                    <span class='glyphicon glyphicon-trash'></span> </button>";
                            }
                            echo "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div> 
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/unitDelete.php');
    }
    $(document).ready(function () {
    });
</script>
