<?php
$sql = "SELECT id,username,fullname FROM user";
$rs = $conn->query($sql);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Username</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($r = $rs->fetch_assoc()) {
                            $i++;
                            ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $r['fullname'] ?></td>
                                <td><?= $r['username'] ?></td>
                                <td><a class="btn btn-primary btn-sm" href="?page=usersEdit&id=<?=$r[id]?>">Edit</a></td>
                            </tr>
                            <?php
                        }
                        ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>