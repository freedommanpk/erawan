<?php
include "lib/php-barcode-generator/BarcodeGenerator.php";
include "lib/php-barcode-generator/BarcodeGeneratorJPG.php";


$condition = isset($_GET['condition']) ? $_GET['condition'] : '';
$category = isset($_GET['categody']) ? $_GET['categody'] : '';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
$factory = isset($_GET['factory']) ? $_GET['factory'] : '';



$sql = "SELECT 
    p.id,
    p.name as product_name,
    p.buy_price,
    p.sell_price,
    p.sell_price_package,
    p.barcode_of_unit,
    p.barcode,
    p.status,
    pk.name as package_name,
    u.name as unit_name,
    c.name as category_name
FROM
    products as p,
    package as pk,
    package_unit as pu,
    unit as u,
    category as c,
    factory as f
WHERE
    p.package_unit_id = pu.id
        AND pu.package_id = pk.id
        AND pu.unit_id = u.id
        AND p.category_id = c.id
        AND p.factory_id = f.id
        AND p.status = 1 ";
if ($condition == 'categody') {
    $sql.= " AND p.category_id = $category";
}

if ($condition == 'keyword') {
    $sql.= " AND p.name like '%$keyword%'";
}

if ($condition == 'factory') {
    $sql.= " AND f.id = $factory";
}

//echo $sql;

$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
$border = 2; //กำหนดความหน้าของเส้น Barcode
$height = 50; //กำหนดความสูงของ Barcode
?>
<div class="row">
    <div class="col-sm-12">
        <form>
            <input type="hidden" value="<?= $_GET['page'] ?>" name="page"/>
            เงื่อนไขการค้นหา: 
            <select name="condition" class="form-control-line">
                <option <?php
                if ($condition === 'all') {
                    echo "selected";
                }
                ?> value="all">ทั้งหมด</option>
                <option <?php
                if ($condition === 'categody') {
                    echo "selected";
                }
                ?> value="categody">ค้นหาตาม Category</option>
                <option <?php
                if ($condition === 'keyword') {
                    echo "selected";
                }
                ?> value="keyword">ค้นหาตาม ชื่อสินค้า</option>
                <option <?php
                if ($condition === 'factory') {
                    echo "selected";
                }
                ?> value="factory">ค้นหาตาม โรงงาน</option>
            </select>
            <span data-role="categody" class="hide">
                เลือก Category: 
                <select name="categody" class="form-control-line">
                    <?php
                    $sqlCat = "SELECT * FROM category ORDER BY name";
                    $rsCat = $conn->query($sqlCat);
                    while ($rCat = $rsCat->fetch_assoc()) {
                        if ($category === $rCat['id']) {
                            $seleted = 'selected';
                        } else {
                            $seleted = '';
                        }
                        ?>
                        <option <?= $seleted ?> value="<?= $rCat['id'] ?>"><?= $rCat['name'] ?> </option>
                        <?php
                    }
                    ?>

                </select>
            </span>
            <span data-role="factory" class="hide">
                เลือก ชื่อโรงงาน: 
                <select name="factory" class="form-control-line">
                    <?php
                    $sqlFac = "SELECT * FROM factory ORDER BY name";
                    $rsFac = $conn->query($sqlFac);
                    while ($rFac = $rsFac->fetch_assoc()) {
                        if ($factory === $rFac['id']) {
                            $seleted = 'selected';
                        } else {
                            $seleted = '';
                        }
                        ?>
                        <option <?= $seleted ?> value="<?= $rFac['id'] ?>"><?= $rFac['name'] . ' - ' . $rFac['code'] ?> </option>
                        <?php
                    }
                    ?>

                </select>
            </span>
            <span data-role="keyword" class="hide">
                คำค้น 
                <input type="text" name="keyword" value="<?= $keyword ?>"/>
            </span>
            <button class="btn btn-sm" type="submit">ค้นหา</button>
        </form>

    </div>
    <div class="col-sm-12">

        <div class="white-box" id='DivIdToPrint'>
            <div class="table-responsive">
                <?php
                if (!$condition) {
                    ?>
                    <div class="panel panel-primary">
                        <div class="panel-body">กรุณาเลือกเงื่อนไขการค้นหา</div>
                    </div>
                    <?php
                } else {
                    ?>



                    <table class="table">
                        <thead>
                            <tr>
                                <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>
                                <th data-column-id="barcode">Barcode Of Package</th>
                                <th data-column-id="sell_price_package" data-type="numeric">Package Sell</th>
                                <th data-column-id="package_name">Package</th>
                                <th data-column-id="barcode_of_unit">Barcode Of Unit</th>
                                <th data-column-id="sell_price" data-type="numeric">Unit Sell</th>
                                <th data-column-id="unit_name">Unit</th>
                                <th data-column-id="product_name">Product Name</th>
                                <th data-column-id="category_name" data-type="numeric">Category</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <?php
                            $rs = $conn->query($sql);
                            $i = 0;
                            while ($r = $rs->fetch_assoc()) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($r['barcode'], $generator::TYPE_CODE_128, $border, $height)) . '">' . '<div  style="text-align:center"><span>' . $r['barcode'] . '</span></div>' ?></td>
                                    <td><?= $r['sell_price_package'] ?></td> 
                                    <td><?= $r['package_name'] ?></td>
                                    <td><?= '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($r['barcode_of_unit'], $generator::TYPE_CODE_128, $border, $height)) . '">' . '<div  style="text-align:center"><span>' . $r['barcode_of_unit'] . '</span></div>' ?></td>
                                    <td><?= $r['sell_price'] ?></td>
                                    <td><?= $r['unit_name'] ?></td>
                                    <td><?= '<strong>' . $r['product_name'] . '</strong>' ?></td>
                                    <td><?= $r['category_name'] ?></td>

                                </tr>
                                <?php
                            }
                            ?>


                        </tbody>
                    </table>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="left" style="text-align: right">
                    <button onclick='printDiv();' id="Print" type="button" class="btn btn-danger">Print</button>
                </div>
            </div> 
        </div>
    </div>
</div>
<script>
    function hideShowCondition(val) {
        if (val == 'categody') {
            $('[data-role="keyword"]').addClass('hide')
            $('[data-role="factory"]').addClass('hide')
            $('[data-role="categody"]').removeClass('hide')
            $('#monthTxt').text($('[name="categody"] option:selected').text())
        } else if (val == 'keyword') {
            $('[data-role="categody"]').addClass('hide')
            $('[data-role="factory"]').addClass('hide')
            $('[data-role="keyword"]').removeClass('hide')
            $('#monthTxt').text($('[name="keyword"] option:selected').text())
        } else if (val == 'factory') {
            $('[data-role="categody"]').addClass('hide')
            $('[data-role="keyword"]').addClass('hide')
            $('[data-role="factory"]').removeClass('hide')
            $('#monthTxt').text($('[name="keyword"] option:selected').text())
        } else {
            $('[data-role="categody"]').addClass('hide')
            $('[data-role="keyword"]').addClass('hide')
            $('[data-role="factory"]').addClass('hide')
        }
    }
    $(document).ready(function () {
        hideShowCondition($('[name="condition"]').val())
        $('[name="condition"]').change(function () {
            var val = $(this).val();
            hideShowCondition(val)
        })
    });
    function printDiv()
    {
        var divToPrint = document.getElementById('DivIdToPrint');
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><head> <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="print"> <link href="css/jquery.bootgrid.min.css" rel="stylesheet" media="print"></head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    }

</script>