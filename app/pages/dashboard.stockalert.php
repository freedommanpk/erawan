<?php
include 'include/function.php';
?>
<div class="white-box">
    <h3 class="box-title">Stock Alert</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NAME</th> 
                    <th>IN STOCK</th>
                    <th>RE ORDER POINT</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "select sa.product_id,p.name, sa.reorder_point,p.package_unit_id from stock_alert as sa, products as p where sa.product_id = p.id";
                $rs_rstock = $conn->query($sql);
                $i = 0;
                while ($r_rstock = $rs_rstock->fetch_assoc()) {
                    $id = $r_rstock['product_id'];
                    $package_unit_id = $r_rstock['package_unit_id'];
                    $remaining = remainingStock($id, $conn);
                    if ($remaining) {
                        $i++;
                        $sqlSelectSetting = "SELECT value_of_unit FROM product_setting WHERE product_id = $id";
                        $rs2 = $conn->query($sqlSelectSetting);
                        $r2 = $rs2->fetch_assoc();

                        $sqlGetPackage = "SELECT pu.package_id, p.name FROM package_unit as pu, package as p  WHERE pu.package_id = p.id AND pu.id = $package_unit_id";
                        $rsGetPackage = $conn->query($sqlGetPackage);
                        $rGetPackage = $rsGetPackage->fetch_assoc();
                        $package_name = $rGetPackage['name'];

                        if ((float) $r_rstock['reorder_point'] >= (float) countRemaining($remaining, $r2['value_of_unit'])) {
                            ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td class="txt-oflo"><?= $r_rstock['name'] ?></td>
                                <td><?php
                                    echo (int) countRemaining($remaining, $r2['value_of_unit']) . ' ' . $package_name;
                                    ?></td>
                                <td class="txt-oflo"><?= $r_rstock['reorder_point'] . ' ' . $package_name ?></td> 
                            </tr>
                            <?php
                        }
                        ?>

                        <?php
                    }
                }
                ?>


            </tbody>
        </table>
    </div>
</div>