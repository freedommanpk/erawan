<?php ?>
<div class="row">
    <div class="col-sm-12">
        <div class="text-right btn-addnew">
            <a href="?page=factoryAdd" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>
                Add New
            </a>
        </div>
        <div class="white-box">
            <div class="table-responsive"> 
                <table id="grid-data-api" class="table table-hover" data-toggle="bootgrid" data-ajax="true" data-url="./api/getfactory.php">
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>                           
                            <th data-column-id="factory_name">Name</th>
                            <th data-column-id="manufacturer">Manufacturer</th>
                            <th data-column-id="tel">Tel</th>
                            <th data-column-id="line_id">Line ID</th>
                            <th data-column-id="action">Action</th>
                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
    </div>
</div>