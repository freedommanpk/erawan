<div class="white-box">
    <h3 class="box-title">Paid Not Deliver</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>                           
                    <th>Invoice Number</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT 
    i.id,
    i.invoice_id,
    i.invoice_credit,
    i.invoice_date,
    i.invoice_due_date,
    i.invoice_payment_status,
    i.invoice_type,
    c.name as customer_name
FROM
    invoice as i,
    customers as c
    
WHERE
     i.customer_id = c.id AND i.invoice_payment_status = 3 ORDER BY i.invoice_date DESC";
                $rs_notpaid = $conn->query($sql);
                $i = 0;
                while ($r = $rs_notpaid->fetch_assoc()) {
                    $i++;
                    $id = $r['id'];

                    $rsSUM = $conn->query("SELECT sum(price) as fullprice FROM invoice_products WHERE invoice_id = $id");
                    $rSUM = $rsSUM->fetch_assoc();
                    ?>
                    <tr>
                        <td><?= $i ?></td>                           
                        <td><?= $r['invoice_id'] ?></td>
                        <td><?= $r['customer_name'] ?></td>
                        <td><?= $r['invoice_date'] ?></td>
                        <td><?= number_format($rSUM['fullprice']) ?></td>
                        <td><?= getStatusText($r['invoice_payment_status']) ?></td>
                        <td>
                            <a title='View/Print Invoice' class='btn btn-default btn-sm' href='?page=invoiceDetail&id=<?= $id ?>'>
                                <span class='glyphicon glyphicon-file'></span>
                            </a>
                            <a title='Edit' class='btn btn-info btn-sm' href='?page=invoiceEdit&id=<?= $id ?>'>
                                <span class='glyphicon glyphicon-edit'></span>
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>