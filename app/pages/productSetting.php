<?php
$p = $_REQUEST['currentPage'];
?>
<div class="row product_setting">
    <div class="col-sm-12"> 
        <div class="white-box">
            <div class="table-responsive"> 
                <table id="grid-data-api" class="table table-hover" data-toggle="bootgrid" data-ajax="true" data-url="./api/getproduct_setting.php">
                <!--<table id="grid" class="table table-hover" >-->
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>
                            <th data-column-id="barcode">Barcode</th>
                            <th data-column-id="product_name">Product Name</th>
                            <!--<th data-column-id="package_name">Package</th>-->
                            <th data-column-id="setting"> </th> 
                            <th data-column-id="action">Action</th>
                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
    </div>
</div> 
<script>
    $(document).ready(function () {
        var grid = $("#grid").bootgrid({
            ajax: true,
            url: "./api/getproduct_setting.php"
        });
    }).on("loaded.rs.jquery.bootgrid", function () {
        $('[data-page="' +<?= $_REQUEST['currentPage'] ?> + '"]').trigger('click');

    })
</script>