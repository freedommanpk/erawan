<?php ?>
<div class="row">
    <div class="col-sm-12">
        <div class="text-right btn-addnew">
            <a href="?page=productsAdd" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>
                Add New
            </a>
        </div>
        <div class="white-box">
            <div class="table-responsive">
                <table id="grid-data-api" class="table table-hover" data-toggle="bootgrid" data-ajax="true" data-url="./api/getproduct.php">
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>
                            <th data-column-id="barcode">Barcode</th>
                            <th data-column-id="product_name">Product Name</th>
                            <th data-column-id="package_name">Package</th>
                            <th data-column-id="unit_name">Unit</th>
                            <th data-column-id="category_name" data-type="numeric">Category</th>
<!--                            <th data-column-id="buy_price" data-type="numeric">Buy</th>
                            <th data-column-id="sell_price" data-type="numeric">Sell</th>-->
                            <th data-column-id="remain"  data-identifier="true">Remain Stock</th> 
                            <th data-column-id="action">Action</th>
                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
    </div>
</div>