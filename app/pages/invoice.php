<?php ?>
<div class="row">
    <div class="col-sm-12">
        <div class="text-right btn-addnew">
<!--            <a href="?page=invoiceReturnProduct" class="btn btn-warning">
                <span class="glyphicon glyphicon-repeat"></span>
                Return Product
            </a>-->
            <a href="?page=invoiceAdd" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>
                Add New
            </a>
        </div>
        <div class="white-box">
            <div class="table-responsive"> 
                <table id="grid-data-api" class="table table-hover" data-toggle="bootgrid" data-ajax="true" data-url="./api/getinvoice.php">
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>                           
                            <th data-column-id="invoice_id">Invoice Number</th>
                            <th data-column-id="customer_name">Customer</th>
                            <th data-column-id="invoice_date">Date</th>
                            <th data-column-id="fullprice">Total</th>
                            <th data-column-id="invoice_payment_status">Status</th>
                            <th data-column-id="action">Action</th>
                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
    </div>
</div>