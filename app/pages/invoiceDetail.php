<?php
include 'include/function.php';
if (!isset($_GET['id'])) {
    exit();
} else {
    $iv_id = $_GET['id'];
}
$sql = "SELECT id, invoice_id, customer_id, invoice_date, invoice_payment_status, 
    invoice_type, invoice_credit, invoice_due_date, note, date_inserted, 
    date_last_update, userId FROM invoice WHERE id = $iv_id; ";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();

$ivCustomer = $r['customer_id'];
$invoice_type = $r['invoice_type'];
$invoice_due_date = $r['invoice_due_date'];

$sqlCustomerByIv = "SELECT * FROM customers WHERE id = $ivCustomer";
$rsCustomerByIv = $conn->query($sqlCustomerByIv);
$rCustomerByIv = $rsCustomerByIv->fetch_assoc();


$sqlProduct = "select
	p.id,
	p.name,
	p.sell_price,
	p.sell_price_package,
	pa.name as package_name,
	u.name as unit_name
from
	products as p,
	package_unit as pu,
	unit as u,
	package as pa
where
	p.package_unit_id = pu.id
	and pu.package_id = pa.id
	and pu.unit_id = u.id";

$rsProduct = $conn->query($sqlProduct);
?>
<style>
    .white-box{
        margin-bottom: 10px;
    }
</style>
<div class="row">

    <div class="col-sm-12">
        <div class="white-box" id='DivIdToPrint'>  
            <div class="row">
                <div class="col-sm-12">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h2><b>ร้านเอราวัณ พลาสติก</b></h2>
                                <div>
                                    <strong>ที่อยู่</strong> : 56/12 ถ.ระนอง ต.ตราดเหนือ อ.เมือง จ.ภูเก็ต 83000
                                </div>
                                <div>
                                    <strong>เบอร์โทรศัพท์</strong> : 08-3521-6565
                                </div>
                            </td>
                            <td align="right">
                                <div style="text-align: right" >
                                    <h1><b>INVOICE</b></h1>
                                </div>
                                <table class="table">
                                    <tr>
                                        <td >
                                            <b>INVOICE ID:</b>  <?= $r['invoice_id'] ?> 
                                        </td>
                                        <td>
                                            <b>DATE:</b>  <?= date('d/m/Y', strtotime($r['invoice_date'])) ?>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <b>Invoice Type:</b>  <?= $invoice_type ?>
                                        </td>
                                        <td>
                                            <b>Due date:</b>  <?= $invoice_due_date == '0000-00-00' ? '-' : $ivCustomer ?>
                                        </td>
                                    </tr> 
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>

            </div> 
            <div class="row">
                <div class="col-sm-12">
                    <div style="background-color: #ecf0f4;
                         padding: 6px;
                         font-size: 16px;
                         font-weight: bold;">
                        BILL TO.
                    </div>
                    <h4>ชื่อ.  <?= $rCustomerByIv['name'] ?> </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    โทร. <?= $rCustomerByIv['phone_number'] ?> 
                </div>
                <div class="col-sm-4">
                    ไลน์. <?= $rCustomerByIv['line_id'] ?> 
                </div>
                <div class="col-sm-4">
                    ที่อยู่. <?= $rCustomerByIv['address'] ?> 
                </div>
            </div>

            <div style="padding-top: 40px"> 
                <!--                <div style="background-color: #ecf0f4;
                                     padding: 6px;
                                     font-size: 16px;
                                     font-weight: bold;">
                                    BILL TO.
                                </div>-->
                <table class="table table-bordered table-responsive" id='tbListOfInvoice'>
                    <thead>
                        <tr style="background-color: #ecf0f4;font-weight: bold">
                            <th style="width:2%">#</th>
                            <th style="width:30%">สินค้า</th>
                            <th style="width:14%">รายละเอียด</th>
                            <th style="width:8%">จำนวน</th>
                            <th style="width:10%">หน่วย</th>
                            <th style="width:10%">ราคาต่อหน่วย</th>
                            <th style="width:10%">รวมเป็นเงิน</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sqlListIvProduct = "SELECT * FROM invoice_products WHERE invoice_id = $iv_id";
                        $rsListIvProduct = $conn->query($sqlListIvProduct);
                        $i = 1;
                        $total = 0;
                        while ($rListIvProduct = $rsListIvProduct->fetch_assoc()) {
                            $product_id = $rListIvProduct['product_id'];
                            $sqlProductName = "SELECT name from products where id = $product_id";
                            $rsProduct = $conn->query($sqlProductName);
                            $rProduct = $rsProduct->fetch_assoc();
                            $product_barcode = $rListIvProduct['product_barcode'];

                            $sqlProReturn = "SELECT * from product_return where barcode = '$product_barcode' and invoice_id = '$iv_id'";
                            $rsProRe = $conn->query($sqlProReturn);
                            $rProRe = $rsProRe->fetch_assoc();
                            ?>
                            <tr data-rol-product="">
                                <td><?= $i ?></td>
                                <td>
                                    <?= $rProduct['name'] ?> [<?= $rListIvProduct['product_barcode'] ?>]
                                    <?php
                                    if ($rProRe['id']) {
                                        ?>
                                        <br/><span style="color:orange;font-size: 12px">ส่งคืนสินค้า จำนวน <?= $rProRe['return_value'] . ' ' . $rProRe['unit'] ?></span>

                                        <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?= $rListIvProduct['description'] ?>
                                </td>
                                <td>
                                    <?= $rListIvProduct['value'] ?>
                                </td>
                                <td>
                                    <?= $rListIvProduct['unit'] ?>
                                </td>
                                <td>
                                    <?= number_format($rListIvProduct['unit_price'], 2) ?>
                                </td>
                                <td>
                                    <?= number_format($rListIvProduct['price'], 2) ?>
                                </td>
                            </tr>
                            <?php
                            $total = $total + (int) $rListIvProduct['price'];
                            $i++;
                        }
                        ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="1"></td>
                            <td colspan="5">
                                หมายเหตุ/บันทึก/เพิ่มเติม : <?= $r['note'] ?>
                            <td colspan="2">
                                <?= number_format($total, 2) ?> บาท</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div> 
        <div class="row">
            <div class="col-sm-6">
                <div class="left" style="text-align: right">
                    <button onclick='printDiv();' id="Print" type="button" class="btn btn-danger">Print</button>
                </div>
            </div> 
        </div>
    </div>
    <script>
        function printDiv()
        {
            var divToPrint = document.getElementById('DivIdToPrint');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><head> <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="print"> <link href="css/jquery.bootgrid.min.css" rel="stylesheet" media="print"><link href="css/print.css" rel="stylesheet" media="print"> </head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 10);
        }

    </script>