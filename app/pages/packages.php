<?php ?>
<div class="row package_page">
    <div class="col-sm-12"> 
        <div class="white-box">
            <form class="form-inline add-new-inline" action="action/packageAdd.php">
                <div class="form-group">
                    <label for="add_new_package">Add New : </label>
                    <input class="form-control" type="text" id="add_new_package" name="value"/>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
            <div class="table-responsive"> 
                <table id="grid-data-api" data-editable="packages" class="table table-hover"  data-ajax="true" data-url="./api/getpackage.php">
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>
                            <th data-column-id="value">Name</th> 
                            <th data-column-id="unit_name">Unit</th> 
                            <th data-column-id="action"></th>  
                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
    </div>
</div>
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/packageDelete.php');
    } 
</script>