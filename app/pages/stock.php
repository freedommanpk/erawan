<?php
include 'include/function.php';
$data = filter_input_array(INPUT_GET);
$barcode = $data['barcode'];
?>
<div class="row package_page">
    <div class="col-sm-12">
        <div class="white-box"> 
            <div>
                <form class="" action="" method="GET">
                    <input type="hidden" name="page" value="stock"/>
                    <div class="form-group">
                        <div class="row" style="
                             padding-bottom: 12px;
                             border-bottom: 1px dashed #e8e8e8; 
                             ">
                            <div class="col-sm-11"> 
                                <label for="package">Barcode :</label>   
                                <input type="number" data-toggle="tooltip" data-placement="top" autocomplete="off" 
                                       title="Please Key Barcode or Scan Barcode and presss 'Enter' to Search the Product" 
                                       class="form-control barcode-input" id="barcode" name="barcode" value="<?= $barcode ?>">                        
                            </div>
                            <div class="col-sm-1" style="    padding-top: 33px;
                                 padding-left: 0px;">
                                <input id="resetBarcode" class="btn btn-default " type="reset" value="Reset" />
                                <!--<button type="reset" name="Reset"/>-->
                            </div>
                        </div>
                    </div>
                </form> 
                <div class="row">
                    <?php
                    if (isset($barcode) && $barcode !== '') {
                        $sql = "SELECT 
                            p.id,
                            p.name as product_name,
                            p.barcode,
                            p.buy_price as buy_price,
                            p.sell_price as sell_price,
                            p.status,
                            pk.name as package_name,
                            u.name as unit_name,
                            f.name as factory_name
                            
                        FROM
                            products as p,
                            package as pk,
                            package_unit as pu,
                            unit as u,
                            factory as f
                        WHERE
                            p.package_unit_id = pu.id
                                AND pu.package_id = pk.id
                                AND pu.unit_id = u.id
                                AND p.status = 1
                                AND p.factory_id = f.id
                                AND (p.barcode = $barcode OR p.barcode_of_unit = $barcode)";

                        $rs = $conn->query($sql);
                        $num_rows = mysqli_num_rows($rs);
                        if ($num_rows >= 1) {
                            $notBigProduct = FALSE;
                            $r = $rs->fetch_assoc();
                            $id = $r['id'];
                            $bigBarcode = $r['barcode'];
                            if ($bigBarcode != $barcode) {
                                $notBigProduct = TRUE;
                            }
                            $sqlSelectSetting = "SELECT value_of_unit FROM product_setting WHERE product_id = $id";
                            $rs2 = $conn->query($sqlSelectSetting);
                            $r2 = $rs2->fetch_assoc();
                            ?>
                            <div style="padding-left: 15px;">
                                <?php
                                if ($notBigProduct) {
                                    echo "<b>Package Barcode:</b> " . $bigBarcode;
                                }
                                ?> 
                            </div>
                            <div class="text-right btn-addnew">
                                <button href="#" class="btn btn-primary" onclick='addValue("<?= $bigBarcode ?>", "<?= $r['package_name'] ?>", "<?= $r['unit_name'] ?>", "<?= $r2['value_of_unit'] ?>")'> 
                                    <span class="glyphicon glyphicon-cloud-upload"></span> In
                                </button>
                                <button href="#" class="btn btn-warning"  onclick='outValue("<?= $bigBarcode ?>", "<?= $r['package_name'] ?>", "<?= $r['unit_name'] ?>", "<?= $r2['value_of_unit'] ?>")'> 
                                    <span class="glyphicon glyphicon-cloud-download"></span> Out
                                </button>
                            </div>
                            <div class="col-sm-4" >
                                <div style="border:solid 1px ghostwhite;padding: 1px">
                                    <table class="table table-hover"> 
                                        <tbody>
                                            <tr>
                                                <td><span style="color:green;font-weight: bold">Factory</span></td>
                                                <td><?= $r['factory_name'] ?></td> 
                                            </tr>
                                            <tr>
                                                <td><span style="color:green;font-weight: bold">Name</span></td>
                                                <td><?= $r['product_name'] ?></td> 
                                            </tr> 
                                            <tr>
                                                <td><span style="color:green;font-weight: bold">Package</span></td>
                                                <td><?= $r['package_name'] ?></td> 
                                            </tr> 
                                            <tr>
                                                <td><span style="color:green;font-weight: bold">Unit</span></td>
                                                <td>
                                                    <?php
                                                    if ($r['unit_name']) {
                                                        $sqlGetBarcodeOfUnit = "SELECT barcode_of_unit FROM products WHERE id = $id";
                                                        $rs1 = $conn->query($sqlGetBarcodeOfUnit);
                                                        $r1 = $rs1->fetch_assoc();
                                                        echo $r['unit_name'] . '<br/>';
                                                        if ($r1['barcode_of_unit']) {
                                                            echo '<b style="color:#f33155"> Unit Barcode : ' . $r1['barcode_of_unit'] . '</b> ';
                                                        }
                                                    }
                                                    ?>
                                                </td> 
                                            </tr> 
                                            <tr>
                                                <td><span style="color:green;font-weight: bold">Remain Stock</span></td>
                                                <td>

                                                    <?php
                                                    $remaining = remainingStock($id, $conn);
                                                    if ($remaining) {
                                                        $sqlQueryStockAlert = "SELECT reorder_point FROM stock_alert WHERE product_id = $id";
                                                        $rs5 = $conn->query($sqlQueryStockAlert);
                                                        $r5 = $rs5->fetch_assoc();

                                                        if ((float) $r5['reorder_point'] >= (float) countRemaining($remaining, $r2['value_of_unit'])) {
                                                            $class = "remaining_red";
                                                        } else {
                                                            $class = "";
                                                        }
                                                        ?>
                                                        <div class="<?= $class ?>">
                                                            <?php
                                                            remainingHTML($remaining, $r2['value_of_unit'], $r['package_name'], $r['unit_name']);
                                                            ?>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?> 
                                                </td> 
                                            </tr> 
                                            <tr>
                                                <td><span style="color:green;font-weight: bold">Buy Price</span></td>
                                                <td><?= $r['buy_price'] ?></td> 
                                            </tr>
                                            <tr>
                                                <td><span style="color:green;font-weight: bold">Sell Price</span></td>
                                                <td><?= $r['sell_price'] ?></td> 
                                            </tr>

                                        </tbody>
                                    </table> 
                                </div> 
                            </div>
                            <div class="col-sm-8 col-lg-8" >
                                <div style="border:solid 1px ghostwhite;padding: 1px">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Doc</th>
                                                <th>Quantity</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sqlSelect = "SELECT `type`,value,date_inserted,doc FROM manage_stock WHERE product_id = $id ORDER BY date_inserted DESC";
                                            $rs4 = $conn->query($sqlSelect);
                                            while ($r4 = $rs4->fetch_assoc()) {
                                                $typeText = inoutOntext($r4['type']);
                                                ?>
                                                <tr>
                                                    <td><span style="color:green;font-weight: bold"><?= $typeText ?></span></td>
                                                    <td><?= $r4['date_inserted'] ?></td>
                                                    <td><?= $r4['doc'] ? $r4['doc'] : '-' ?></td>
                                                    <td><?= $r4['value'] ?></td> 
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-danger" role="alert">
                                <strong>โอ้! </strong> ไม่มีสินค้านี้นะ ลองตรวจสอบบาร์โค้ดใหม่~~~
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                    } else {
                        ?>
                        <div class="alert alert-info" role="alert">
                            <strong>Wait a minute! </strong>Please scan the barcode first.
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#barcode').focus();
        $('#resetBarcode').click(function () {
            if ($('#barcode').val()) {
                window.location = '?page=stock';
            }
        })
    });
    function addValue(id, package_name, unit_name, value_of_unit) {
        if (value_of_unit) {
            $('#addToStock').modal();
            $('#addToStock #value').focus();
            $('#addToStock #barcode_id').val(id);
            $('#addToStock #barcode_id_span').text(id);
            $('#addToStock #package_name').text(package_name);
            $('#addToStock #unit_name').text(unit_name);
            $('#addToStock #value_of_unit').text(value_of_unit);
            $('#addToStock #barcode-page_item').attr('action', 'action/stockAdd.php');
        } else {
            alert('Not have Value of Unit');
        }
    }
    function outValue(id, package_name, unit_name, value_of_unit) {
        if (value_of_unit) {
            $('#outOfStock').modal();
            $('#outOfStock #value').focus();
            $('#outOfStock #barcode_id').val(id);
            $('#outOfStock #barcode_id_span').text(id);
            $('#outOfStock #package_name').text(package_name);
            $('#outOfStock #unit_name').text(unit_name);
            $('#outOfStock #value_of_unit').text(value_of_unit);
            $('#outOfStock #barcode-page_item').attr('action', 'action/stockOut.php');
        } else {
            alert('Not have Value of Unit');
        }
    }
</script>