<?php ?>
<div class="row">
    <div class="col-sm-12">

        <div class="white-box" id='DivIdToPrint'>
            <div class="table-responsive">
                <table id="grid-data-api" class="table table-hover" data-toggle="bootgrid" data-ajax="true" data-url="./api/getproduct_report.php">
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>
                            <th data-column-id="barcode">Barcode Of Package</th>
                            <th data-column-id="sell_price_package" data-type="numeric">Package Sell</th>
                            <th data-column-id="package_name">Package</th>
                            <th data-column-id="barcode_of_unit">Barcode Of Unit</th>
                            <th data-column-id="sell_price" data-type="numeric">Unit Sell</th>
                            <th data-column-id="unit_name">Unit</th>
                            <th data-column-id="product_name">Product Name</th>
                            <th data-column-id="category_name" data-type="numeric">Category</th>

                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="left" style="text-align: right">
                    <button onclick='printDiv();' id="Print" type="button" class="btn btn-danger">Print</button>
                </div>
            </div> 
        </div>
    </div>
</div>
<script>
    function printDiv()
    {
        var divToPrint = document.getElementById('DivIdToPrint');
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><head> <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="print"> <link href="css/jquery.bootgrid.min.css" rel="stylesheet" media="print"><link href="css/print.css" rel="stylesheet" media="print"> </head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    }
</script>