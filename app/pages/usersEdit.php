<?php
$id = $_GET['id'];

$sql = "SELECT username, fullname, password FROM user WHERE id = $id";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form autocomplete="off" action="action/userEdit.php" method="post">
                <input type="hidden" name='id' value="<?= $id ?>" />
                <div class="form-group">
                    <label for="fullname">Full Name:</label>
                    <input type="text" class="form-control" id="fullname" name="fullname" value="<?= $r['fullname'] ?>">
                </div>
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?= $r['username'] ?>">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" name="pwd" value="<?= $r['password'] ?>">
                </div> 
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>