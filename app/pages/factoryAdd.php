<?php
include 'include/function.php';
$sql = "SELECT count(code) as co FROM factory";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();
$count = $r['co'];
if ($count < 1000) {
    $rTxt = (string) ($count + 1);
}
if ($count < 100) {
    $rTxt = '0' . (string) ($count + 1);
}
if ($count < 10) {
    $rTxt = '00' . (string) ($count + 1);
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form autocomplete="off" action="action/factoryAdd.php" method="post" class="form-horizontal form-material">
                <input type="hidden" name='id' value="<?= $id ?>" />
                <input type="hidden" name="unit_id" id="unit_id"/> 
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="name">Code:</label>
                        <input type="text" class="form-control" id="code" name="code" value="<?= $rTxt ?>" >
                    </div>
                    <div class="col-sm-3">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" >
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Manufacturer:</label>
                        <input type="text" class="form-control" id="manufacturer" name="manufacturer" >
                    </div>               
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="name">Tel:</label>
                        <input type="text" class="form-control" id="tel" name="tel" >
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Line ID:</label>
                        <input type="text" class="form-control" id="line_id" name="line_id" >
                    </div>      
                </div>
                <div class="row"> 
                    <div class="col-sm-3"> 
                        <label for="package">House number:</label>  
                        <input type="text" class="form-control" id="house_number" name="house_number" >
                    </div>
                    <div class="col-sm-3"> 
                        <label for="package">Road:</label>  
                        <input type="text" class="form-control" id="road" name="road" >
                    </div>
                    <div class="col-sm-3"> 
                        <label for="barcode">District:</label>
                        <input type="text" class="form-control " id="district" name="district">
                    </div>
                    <div class="col-sm-3"> 
                        <label for="package">Sub District:</label>  
                        <input type="text" class="form-control" id="sub_district" name="sub_district" >
                    </div>

                </div> 
                <div class="row"> 
                    <div class="col-sm-3"> 
                        <label for="unit">Moo:</label> 
                        <input type="text" class="form-control" id="moo" name="moo" >                        
                    </div>
                    <div class="col-sm-3"> 
                        <label for="barcode_of_unit">Province:</label> 
                        <input type="text" class="form-control" id="province" name="province" >
                    </div>
                    <div class="col-sm-3">
                        <label for="buy">Country:</label>
                        <input type="text" class="form-control" id="country" name="country"  />
                    </div>
                    <div class="col-sm-3">
                        <label for="sell">Zip Code:</label>
                        <input type="text" class="form-control" id="zip_code" name="zip_code" />
                    </div>
                </div> 
                <div class="row"> 
                    <h3>Factory Sales Executive</h3>
                    <div class="col-xs-6"> 
                        <label for="sale_name_ex1">Name:</label>
                        <input type="text" class="form-control" id="sale_name_ex1" name="sale_name_ex1" />
                    </div>
                    <div class="col-xs-6"> 
                        <label for="sale_phone_ex1">Telephone:</label>
                        <input type="text" class="form-control" id="sale_phone_ex1" name="sale_phone_ex1" />
                    </div>
                    <div class="col-xs-6"> 
                        <label for="sale_name_ex2">Name:</label>
                        <input type="text" class="form-control" id="sale_name_ex2" name="sale_name_ex2" />
                    </div>
                    <div class="col-xs-6"> 
                        <label for="sale_phone_ex2">Telephone:</label>
                        <input type="text" class="form-control" id="sale_phone_ex2" name="sale_phone_ex2" />
                    </div>
                </div>

                <div class="row"> 

                    <div class="col-xs-12"> 
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                        <a onclick="history.back();" class="btn btn-default pull-right" style="margin-right: 10px">Cancel</a>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/productDelete.php');
    }
    $(document).ready(function () {
        function fetchUnit(id) {
            $.ajax({url: "api/getunit.php?package_id=" + id + "", success: function (result) {
                    $('#unit').empty();
                    $('#unitlist').empty();
                    var dfUnit_id = 0;
                    if ($('#unit_id').val()) {
                        dfUnit_id = $('#unit_id').val();
                    }
                    var data = JSON.parse(result);
                    data.forEach(function (v) {
//                        var html = "<div class='checkbox'> <label><input type='checkbox' value='" + v.id + "'>" + v.name + "</label></div>";
                        if (+dfUnit_id === +v.id) {
                            $('#unit').append("<option selected value='" + v.id + "'>" + v.name + "</option>");
//                            $('#unitlist').append(html);
                            $('#unit_display_name').html(v.name);
                        } else {
//                            $('#unitlist').append(html);
                            $('#unit').append("<option value='" + v.id + "'>" + v.name + "</option>");
                        }
                    });
                }
            });
        }
        if ($('#packages').val()) {
            fetchUnit($('#packages').val());
        }
        $('#packages').change(function () {
            var id = $(this).val();
            fetchUnit(id);
        });

        $('#unit').change(function () {
            $('#unit_display_name').html($("#unit option:selected").text());
        });
    });
</script>
