<?php ?>
<div class="row">
    <div class="col-sm-12">
        <div class="text-right btn-addnew">
            <a href="?page=customerAdd" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>
                Add New
            </a>
        </div>
        <div class="white-box">
            <div class="table-responsive">
                <table id="grid-data-api" class="table table-hover" data-toggle="bootgrid" data-ajax="true" data-url="./api/getcustomer.php">
                    <thead>
                        <tr>
                            <th data-column-id="numrow" data-type="numeric" data-identifier="true">#</th>
                            <th data-column-id="customers_name">Name</th>
                            <th data-column-id="phone_number">PhoneNumber</th>
                            <th data-column-id="line_id">Line_id</th>
                            <th data-column-id="address">Address</th>     
                            <th data-column-id="action">Action</th>
                        </tr>
                    </thead> 
                </table>

            </div>
        </div>
    </div>
</div>
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/customerDelete.php');
    } 
</script>