<?php
include 'include/function.php';
$id = $_GET['id'];
$sql = "SELECT id,name,manufacturer,tel,line_id,house_number,road,district,sub_district,moo,province,country,zip_code "
        . "FROM factory WHERE id = $id";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();

$sqlsale = "SELECT name, phone FROM  factorie_sales where factory_id = '$id'";
$rssale = $conn->query($sqlsale);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form autocomplete="off" action="action/factoryEdit.php" method="post" class="form-horizontal form-material">
                <input type="hidden" name='id' value="<?= $r['id'] ?>" />  
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?= $r['name'] ?>">
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Manufacturer:</label>
                        <input type="text" class="form-control" id="manufacturer" name="manufacturer" value="<?= $r['manufacturer'] ?>" >
                    </div>               
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="name">Tel:</label>
                        <input type="text" class="form-control" id="tel" name="tel" value="<?= $r['tel'] ?>">
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Line ID:</label>
                        <input type="text" class="form-control" id="line_id" name="line_id" value="<?= $r['line_id'] ?>">
                    </div>      
                </div>
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-3"> 
                            <label for="package">House number:</label>  
                            <input type="text" class="form-control" id="house_number" name="house_number" value="<?= $r['house_number'] ?>">
                        </div>
                        <div class="col-sm-3"> 
                            <label for="package">Road:</label>  
                            <input type="text" class="form-control" id="road" name="road" value="<?= $r['road'] ?>">
                        </div>
                        <div class="col-sm-3"> 
                            <label for="barcode">District:</label>
                            <input type="text" class="form-control " id="district" name="district" value="<?= $r['district'] ?>">
                        </div>
                        <div class="col-sm-3"> 
                            <label for="package">Sub District:</label>  
                            <input type="text" class="form-control" id="sub_district" name="sub_district" value="<?= $r['sub_district'] ?>">
                        </div>

                    </div>
                </div> 
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-3"> 
                            <label for="unit">Moo:</label> 
                            <input type="text" class="form-control" id="moo" name="moo" value="<?= $r['moo'] ?>">                        
                        </div>
                        <div class="col-sm-3"> 
                            <label for="barcode_of_unit">Province:</label> 
                            <input type="text" class="form-control" id="province" name="province" value="<?= $r['province'] ?>">
                        </div>
                        <div class="col-sm-3">
                            <label for="buy">Country:</label>
                            <input type="text" class="form-control" id="country" name="country"  value="<?= $r['country'] ?>">
                        </div>
                        <div class="col-sm-3">
                            <label for="sell">Zip Code:</label>
                            <input type="text" class="form-control" id="zip_code" name="zip_code" value="<?= $r['zip_code'] ?>">
                        </div>
                    </div> 
                    <div class="row"> 
                        <div class="col-xs-12"> 
                            <h3>Factory Sales Executive</h3>
                        </div>
                        <?php
                        $i = 0;
                        while ($rsale = $rssale->fetch_assoc()) {
                            $i++;
                            if ($i == 1) {
                                $sale_name_ex1 = $rsale['name'];
                                $sale_phone_ex1 = $rsale['phone'];
                            }
                            if ($i == 2) {
                                $sale_name_ex2 = $rsale['name'];
                                $sale_phone_ex2 = $rsale['phone'];
                            }
                        }
                        ?>

                        <div class="col-xs-6"> 
                            <label for="sale_name_ex1">Name:</label>
                            <input type="text" class="form-control" id="sale_name_ex1" name="sale_name_ex1" value="<?= $sale_name_ex1 ?>" />
                        </div>
                        <div class="col-xs-6"> 
                            <label for="sale_phone_ex1">Telephone:</label>
                            <input type="text" class="form-control" id="sale_phone_ex1" name="sale_phone_ex1" value="<?= $sale_phone_ex1 ?>" />
                        </div>
                        <div class="col-xs-6"> 
                            <label for="sale_name_ex2">Name:</label>
                            <input type="text" class="form-control" id="sale_name_ex2" name="sale_name_ex2" value="<?= $sale_name_ex2 ?>" />
                        </div>
                        <div class="col-xs-6"> 
                            <label for="sale_phone_ex2">Telephone:</label>
                            <input type="text" class="form-control" id="sale_phone_ex2" name="sale_phone_ex2" value="<?= $sale_phone_ex2 ?>" />
                        </div>
                    </div>
                </div> 
                <div class="row"> 
                    <div class="col-xs-6">
                        <a onclick="confirmDelete(<?= $id ?>)" type="button" class="btn btn-danger">Delete</a>
                    </div>
                    <div class="col-xs-6"> 
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                        <a onclick="history.back();" class="btn btn-default pull-right" style="margin-right: 10px">Cancel</a>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/factoryDelete.php');
    }
    $(document).ready(function () {

    });
</script>
