<?php
include 'include/function.php';
$id = $_GET['id'];
$remaining = remainingStock($id, $conn);

$sql = "SELECT barcode,barcode_of_unit,name,package_unit_id,buy_price,sell_price,sell_price_package,factory_id,category_id,status FROM products WHERE id = $id";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();
$package_unit_id = $r['package_unit_id'];



if (!$r['status']) {
    echo '<script type="text/javascript">
           window.location = "?page=products"
      </script>';
}

$sql1 = "SELECT id,name FROM package WHERE status = 1 ORDER BY name asc";
$rs1 = $conn->query($sql1);

$sql2 = "SELECT package_id, unit_id FROM package_unit WHERE id = $package_unit_id AND status = 1";
$rs2 = $conn->query($sql2);
$r2 = $rs2->fetch_assoc();
$rpackage_id = $r2['package_id'];
$runit_id = $r2['unit_id'];

$sql3 = "SELECT id, name,manufacturer FROM factory";
$rs3 = $conn->query($sql3);

$sql4 = "SELECT id, name,code FROM category";
$rs4 = $conn->query($sql4);

$sql5 = "SELECT id,reorder_point FROM stock_alert WHERE product_id = $id ";
$rs5 = $conn->query($sql5);
$r5 = $rs5->fetch_assoc();
$reorder_point_id = $r5['id'];
$reorder_point = $r5['reorder_point'];



?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <form id="formProductEdit" autocomplete="off" action="action/productEdit.php" method="post" class="form-horizontal form-material" onsubmit="return false">
                <input type="hidden" name='id' value="<?= $id ?>" />
                <input type="hidden" name="unit_id" id="unit_id" value="<?= $r2['unit_id'] ?>"/>

                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?= $r['name'] ?>">
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="factory">Factory:</label>
                        <select class="form-control" id="factory"  name="factory" >
                            <option value="">--- Select ---</option>
                            <?php
                            while ($r3 = $rs3->fetch_assoc()) {
                                if ($r['factory_id'] == $r3['id']) {
                                    $selection = 'selected';
                                } else {
                                    $selection = '';
                                }
                                ?>
                                <option <?= $selection ?> value="<?= $r3['id'] ?>"><?= $r3['name'] ?> (<?= $r3['manufacturer'] ?>)</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="name">Category:</label> 
                        <select class="form-control" id="category"  name="category"  required>
                            <option value="">--- Select ---</option>
                            <?php
                            while ($r4 = $rs4->fetch_assoc()) {
                                if ($r['category_id'] == $r4['id']) {
                                    $selection = 'selected';
                                } else {
                                    $selection = '';
                                }
                                ?>
                                <option <?= $selection ?> value="<?= $r4['id'] ?>"><?= $r4['code'] . ' ' . $r4['name'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6"> 
                            <label for="package">Package:</label>  
                            <select class="form-control " id="packages"  name="packages" >
                                <option value="">--- Select ---</option>
                                <?php
                                while ($r1 = $rs1->fetch_assoc()) {
                                    if ($r1['id'] == $r2['package_id']) {
                                        $selection = 'selected';
                                    } else {
                                        $selection = '';
                                    }
                                    ?>
                                    <option <?= $selection ?> value="<?= $r1['id'] ?>"><?= $r1['name'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-6"> 
                            <label for="barcode">Barcode of Package:</label>
                            <input type="text" class="form-control " id="barcode" name="barcode" value="<?= $r['barcode'] ?>">
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6"> 
                            <label for="unit">Unit:</label> 
                            <select class="form-control" id="unit"  name="unit" >  </select> 
                        </div>
                        <div class="col-sm-6"> 
                            <label for="barcode_of_unit">Barcode of Unit:</label> 
                            <input type="text" class="form-control" id="barcode_of_unit" name="barcode_of_unit" value="<?= $r['barcode_of_unit'] ?>">
                        </div>
                    </div> 
                </div> 
                <div class="form-group">
                    <label for="buy">Buy Price:</label>
                    <input type="text" class="form-control" id="buy" name="buy" value="<?= $r['buy_price'] ?>">
                </div>
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6"> 
                            <label for="sell">Sell Price Unit:</label>
                            <input type="text" class="form-control" id="sell" name="sell" value="<?= $r['sell_price'] ?>">
                        </div>
                        <div class="col-sm-6"> 
                            <label for="sell">Sell Price Package:</label>
                            <input type="text" class="form-control" id="sell_package" name="sell_package" value="<?= $r['sell_price_package'] ?>">
                        </div>
                    </div>
                </div>
                <?php
                $sqlSelectSetting = "SELECT value_of_unit FROM product_setting WHERE product_id = $id";
                $rsSt = $conn->query($sqlSelectSetting);
                $rSt = $rsSt->fetch_assoc();

                $sqlPk = "SELECT name as package_name FROM package WHERE id = $rpackage_id";
                $rsPk = $conn->query($sqlPk);
                $rPk = $rsPk->fetch_assoc();

                $sqlUn = "SELECT name as unit_name   FROM unit WHERE id = $runit_id";
                $rsUn = $conn->query($sqlUn);
                $rUn = $rsUn->fetch_assoc();
                ?>
                <div class="form-group">
                    <div class="row"> 
                        <div class="col-sm-6">
                            <label for="remain">Reorder Point: (<?= $rPk['package_name']?>)</label>
                            <input type="hidden" value="<?= $reorder_point_id ?>" name="reorder_point_id" />
                            <input type="text" class="form-control" id="reorder_point" name="reorder_point" value="<?= $reorder_point ?>">
                        </div>
                        <div class="col-sm-6"> 
                            <label for="remain">Remaining Stock:</label>
                            <label>
                                <?php
                                $remaining = remainingStock($id, $conn);
                                if ($remaining) {
                                    remainingHTML($remaining, $rSt['value_of_unit'], $rPk['package_name'], $rUn['unit_name']);
                                } else {
                                    echo "-";
                                }
                                ?> 
                            </label>
                            <!--<label><?= $remaining ?> <span id="unit_display_name"></span></label>-->
                        </div>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-xs-6">
                        <a onclick="confirmDelete(<?= $id ?>)" type="button" class="btn btn-danger">Delete</a>
                    </div>
                    <div class="col-xs-6"> 
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                        <a onclick="history.back();" class="btn btn-default pull-right" style="margin-right: 10px">Cancel</a>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 
<script>
    function confirmDelete(id) {
        $('#confirmDeleteModal').modal();
        $('#id_item').val(id);
        $('#page_item').attr('action', 'action/productDelete.php');
    }
    $(document).ready(function () {
        $('#category,#factory').select2( )
        $('#formProductEdit').submit(function () {
            var data = objectifyForm($(this));
            $.ajax({
                type: 'POST',
                data: data,
                url: 'action/productEdit.php',
                success: function (data) {
                    if (data) {
                        $.notify({
                            message: 'Edit done'
                        }, {
                            type: 'info',
                            delay: 200,
                            showProgressbar: true,
                            onClose: function () {

                            }
                        })
                    }
                }
            });
        })



        function fetchUnit(id) {
            $.ajax({url: "api/getunit.php?package_id=" + id + "", success: function (result) {
                    $('#unit').empty();
                    $('#unitlist').empty();
                    var dfUnit_id = 0;
                    if ($('#unit_id').val()) {
                        dfUnit_id = $('#unit_id').val();
                    }
                    var data = JSON.parse(result);
                    data.forEach(function (v) {
//                        var html = "<div class='checkbox'> <label><input type='checkbox' value='" + v.id + "'>" + v.name + "</label></div>";
                        if (+dfUnit_id === +v.id) {
                            $('#unit').append("<option selected value='" + v.id + "'>" + v.name + "</option>");
//                            $('#unitlist').append(html);
                            $('#unit_display_name').html(v.name);
                        } else {
//                            $('#unitlist').append(html);
                            $('#unit').append("<option value='" + v.id + "'>" + v.name + "</option>");
                        }
                    });
                }
            });
        }
        if ($('#packages').val()) {
            fetchUnit($('#packages').val());
        }
        $('#packages').change(function () {
            var id = $(this).val();
            fetchUnit(id);
        });

        $('#unit').change(function () {
            $('#unit_display_name').html($("#unit option:selected").text());
        });
    });
</script>
