<?php

require '../../config/connection.php';
/* @var $username type */
$customer = isset($_POST['customer']) ? $_POST['customer'] : '';
$date = isset($_POST['date']) ? $_POST['date'] : '';
$duedate = isset($_POST['duedate']) ? $_POST['duedate'] : '';
$note = isset($_POST['note']) ? $_POST['note'] : '';
$term = isset($_POST['term']) ? $_POST['term'] : '';
$total = isset($_POST['total']) ? $_POST['total'] : '';
$productList = isset($_POST['productList']) ? $_POST['productList'] : '';

// Convert Date
$date_c = date('Y-m-d', strtotime(str_replace('/', '-', $date)));

$duedate_c = null;
$invoice_credit = 0;
if ($duedate) {
    $invoice_credit = 1;
    $duedate_c = date('Y-m-d', strtotime(str_replace('/', '-', $duedate)));
}
// generate invoice id 

$sql = "SELECT invoice_id FROM invoice ORDER BY id DESC LIMIT 1";
$rs = $conn->query($sql);
$r = $rs->fetch_assoc();
$lastIV = $r['invoice_id'];

$firstIVchar = 'IV';
$yymm = date("ym");
$firstIV = '0001';


if (!$lastIV) {
    $genIV = $firstIVchar . $yymm . $firstIV;
} else {
    $monthIV = (int) substr($lastIV, 4, 2);
    if ($monthIV == date("m")) {
        $strLastIV = (int) substr($lastIV, 6);
        $genIV = 0;
        if ($strLastIV >= 1 && $strLastIV <= 9) {
            $genIV = $firstIVchar . $yymm . '000' . (String) ($strLastIV + 1);
        }

        if ($strLastIV >= 10 && $strLastIV <= 99) {
            $genIV = $firstIVchar . $yymm . '00' . (String) ($strLastIV + 1);
        }

        if ($strLastIV >= 100 && $strLastIV <= 999) {
            $genIV = $firstIVchar . $yymm . '0' . (String) ($strLastIV + 1);
        }

        if ($strLastIV >= 1000 && $strLastIV <= 9999) {
            $genIV = $firstIVchar . $yymm . (String) ($strLastIV + 1);
        }
    } else {
        $genIV = $firstIVchar . $yymm . $firstIV;
    }
}

$sqlInsertIV = "INSERT INTO invoice
(invoice_id, invoice_date, invoice_payment_status, invoice_type, invoice_credit, invoice_due_date,note,customer_id)
VALUES('$genIV','$date_c', 0, '$term', $invoice_credit, '$duedate_c','$note',$customer);";


if ($conn->query($sqlInsertIV)) {
    $invoice_id = $conn->insert_id;
    foreach ($productList as $value) {
        $product_id = $value['id'];
        $product_barcode = $value['product_barcode'];
        $description = $value['description'];
        $valuex = $value['value'];
        $unit = $value['unit'];
        $unit_price = $value['unit_price'];
        $price = $value['price'];
        $sqlInsertIVProduct = "INSERT INTO invoice_products
(product_id, product_barcode, invoice_id, value, unit, unit_price, price, description)
VALUES($product_id, '$product_barcode',$invoice_id, $valuex, '$unit', $unit_price, $price, '$description');";
        $conn->query($sqlInsertIVProduct);
    }
    echo 1;
}

