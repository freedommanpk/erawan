<?php

require '../../config/connection.php';
/* @var $username type */
$name = $_REQUEST['name'];
$factory = $_REQUEST['factory'];
$category = $_REQUEST['category'];
$package = $_REQUEST['packages'];
//$barcode_of_package = $_REQUEST['barcode_of_package'];
$unit = $_REQUEST['unit'];
//$barcode_of_unit = $_REQUEST['barcode_of_unit'];
$buy_price = $_REQUEST['buy_price'];
$sell_price = $_REQUEST['sell_price'];
$sell_package = $_REQUEST['sell_package'];

$reorder_point = $_REQUEST['reorder_point'];
// get factory code
$factory_code_sql = "Select code from factory where id = $factory";
$rs3 = $conn->query($factory_code_sql);
$r3 = $rs3->fetch_assoc();
$factory_code = $r3['code'];

// get category code
$category_code_sql = "Select code from category where id = $category";
$rs4 = $conn->query($category_code_sql);
$r4 = $rs4->fetch_assoc();
$category_code = $r4['code'];

$concatFacCat = $factory_code . $category_code;
$count_code_sql = "SELECT  count(id)+1 as `count` FROM `products` WHERE SUBSTRING(barcode,1,6) = '$concatFacCat'";
$rs5 = $conn->query($count_code_sql);
$r5 = $rs5->fetch_assoc();
$count_code = $r5['count'];
$count_code_text = '';

if ($count_code < 10) {
    $count_code_text = '000' . $count_code;
} else if ($count_code < 100) {
    $count_code_text = '00' . $count_code;
} else if ($count_code < 1000) {
    $count_code_text = '0' . $count_code;
} else if ($count_code < 10000) {
    $count_code_text = $count_code;
} else {
    echo json_encode(array(
        "status" => "barcode_max"
    ));
    exit();
}

$barcode_of_package = $concatFacCat . $count_code_text;
$barcode_of_unit = $barcode_of_package . '1';

$sql2 = "SELECT COUNT(id) as value FROM products  WHERE barcode = $barcode_of_package ";
$rs2 = $conn->query($sql2);
$r2 = $rs2->fetch_assoc();
if ($r2['value'] <= 0) {
    $sql1 = "SELECT id FROM package_unit WHERE package_id= $package AND unit_id = $unit";
    $rs = $conn->query($sql1);
    $r = $rs->fetch_assoc();
    $package_unit_id = $r['id'];
    $sql = "INSERT INTO products (name,factory_id,category_id,barcode,barcode_of_unit,buy_price,sell_price,sell_price_package,package_unit_id) "
            . "VALUES ('$name','$factory','$category','$barcode_of_package','$barcode_of_unit','$buy_price','$sell_price','$sell_package','$package_unit_id')";

    if ($conn->query($sql)) {
        $last_id = $conn->insert_id;
        $sqlal = "INSERT INTO stock_alert (product_id,reorder_point) VALUES ($last_id, $reorder_point)";
        $conn->query($sqlal);
        echo json_encode(array(
            "status" => "success",
            "barcode_of_package" => $barcode_of_package,
            "barcode_of_unit" => $barcode_of_unit
        ));
    } else {
        echo json_encode(array(
            "status" => "fail"
        ));
    }
} else {
    echo json_encode(array(
        "status" => "dup"
    ));
}