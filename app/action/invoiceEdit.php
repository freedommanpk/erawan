<?php

require '../../config/connection.php';
/* @var $username type */
$id = isset($_POST['id']) ? $_POST['id'] : '';
$date = isset($_POST['date']) ? $_POST['date'] : '';
$duedate = isset($_POST['duedate']) ? $_POST['duedate'] : '';
$note = isset($_POST['note']) ? $_POST['note'] : '';
$term = isset($_POST['term']) ? $_POST['term'] : '';
$total = isset($_POST['total']) ? $_POST['total'] : '';
$productList = isset($_POST['productList']) ? $_POST['productList'] : '';

// Convert Date
$date_c = date('Y-m-d', strtotime(str_replace('/', '-', $date)));

$duedate_c = null;
$invoice_credit = 0;
if ($duedate) {
    $invoice_credit = 1;
    $duedate_c = date('Y-m-d', strtotime(str_replace('/', '-', $duedate)));
}

//Update Invoice
$sqlUpdateIV = "UPDATE invoice
SET invoice_date='$date_c', invoice_type='$term', invoice_credit=$invoice_credit, 
    invoice_due_date='$duedate_c', note='$note' WHERE id=$id;";


if ($conn->query($sqlUpdateIV)) {
    //Delete all product in invoice 
    $sqldelIV = "DELETE from invoice_products WHERE invoice_id = $id";
    $conn->query($sqldelIV);
    $invoice_id = $id;
    foreach ($productList as $value) {
        $product_barcode = $value['product_barcode'];
        $product_id = $value['id'];
        $description = $value['description'];
        $valuex = $value['value'];
        $unit = $value['unit'];
        $unit_price = $value['unit_price'];
        $price = $value['price'];
        $sqlInsertIVProduct = "INSERT INTO invoice_products
(product_id, product_barcode, invoice_id, value, unit, unit_price, price, description)
VALUES($product_id, '$product_barcode',$invoice_id, $valuex, '$unit', $unit_price, $price, '$description');";
        $conn->query($sqlInsertIVProduct);
    }
    echo 1;
}

