<?php

require '../../config/connection.php';
/* @var $username type */
$value = $_REQUEST['value'];
if (!empty($value)) {

    $sqlCheck = "SELECT max(code)+1 as nextCode FROM `category`";
    $rs = $conn->query($sqlCheck);
    $r = $rs->fetch_assoc();
    $nextCode = (int) $r['nextCode'];

    if ($nextCode < 1000) {
        $rTxt = (string) ($nextCode);
    }
    if ($nextCode < 100) {
        $rTxt = '0' . (string) ($nextCode);
    }
    if ($nextCode < 10) {
        $rTxt = '00' . (string) ($nextCode);
    }

    $sql = "INSERT INTO category (code,name) VALUES ('$rTxt','$value')";
    if ($conn->query($sql)) {
        echo "<script>history.back(); </script>";
    } else {
        echo "<script>alert('ERR!');</script>";
    }
} else {
    echo "<script>alert('This field is required!');</script>";
    echo "<script>history.back(); </script>";
}

