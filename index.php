<?php

session_start();
if (empty($_SESSION["user_id"])) {
    header("Location: ./login");
    die();
} else {
    header("Location: ./app");
    die();
}
